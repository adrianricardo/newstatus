<?php

class Ajax extends Front_Controller {

	function __construct()
	{
		parent::__construct();
		//make sure we're not always behind ssl
		remove_ssl();
	}
	function index()
	{
		$slug = $this->input->post('category');
		$offset = $this->input->post('offset');
		$products	= $this->Product_model->filter_products($slug,20,$offset,'products.id','DESC');
		if(empty($products)){
			$products['empty'] = true;
			echo json_encode($products);
		}else{
			echo json_encode($products);
		}
	}
	function search($code=false, $page = 0)
	{
		$this->load->model('Search_model');
		
		//check to see if we have a search term
		//if(!$code)
		{
			//if the term is in post, save it to the db and give me a reference
			$term		= $this->input->post('term', true);
			$offest		= $this->input->post('offset', true);
			$code		= $this->Search_model->record_term($term);
			
			// no code? redirect so we can have the code in place for the sorting.
			// I know this isn't the best way...
			//redirect('cart/search/'.$code.'/'.$page);
		}
		//else
		{
			//if we have the md5 string, get the term
			$term	= $this->Search_model->get_term($code);
		}
		
		if(empty($term))
		{
			//if there is still no search term throw an error
			//if there is still no search term throw an error
			//$this->session->set_flashdata('error', lang('search_error'));
			header('HTTP/1.1 400 Bad Request', true, 400);
			return false;
		}
 		

		if(empty($term))
		{
			//if there is still no search term throw an error
			$this->load->view('search_error', $data);
		}
		else
		{
	
 		
			//set up pagination
			$this->load->library('pagination');
			$config['base_url']		= base_url().'cart/search/'.$code.'/';
			$config['uri_segment']	= 4;
			$config['per_page']		= 20;
			
			
			$result					= $this->Product_model->search_products($term, 20, $offest,false ,false );
			$config['total_rows']	= $result['count'];
 	
			echo json_encode($result);
			/*foreach ($data['products'] as &$p)
			{
				$p->images	= (array)json_decode($p->images);
				$p->options	= $this->Option_model->get_product_options($p->id);
			}*/
			//$this->load->view('category', $data);
		}
	}
	function facebookLogin(){
		require_once("./fb-sdk/facebook.php");

		$config = array();
		$config['appId'] = '682397635109272';
		$config['secret'] = '506ea6ec12948370274091a311a965b1';

		$facebook = new Facebook($config);
		$user_id = $facebook->getUser();

		//find out if they're already logged in, if they are redirect them to the my account page
		$redirect	= $this->Customer_model->is_logged_in(false, false);
		//if they are logged in, we send them back to the my_account by default, if they are not logging in
		if ($redirect)
		{
			redirect('secure/my_account/');
		}
		
 		if($user_id) {

	      // We have a user ID, so probably a logged in user.
	      // If not, we'll get an exception, which we handle below.
	      try {

	        $user_profile = $facebook->api('/me','GET');
	        //if this email has an account, log in
	        if($this->Customer_model->isUser($user_profile['id'])){
	        	$login = $this->Customer_model->fblogin($user_profile['id']);
	        	if ($login){
	        		$this->session->set_flashdata('message', "Welcome back ".$user_profile['first_name']);
	        		redirect('/');
	        	}else{

	        		redirect('secure/login/');
	        	}
	        }
	        else{//else register the user 

	        	$save['id']		= false;
			
				$save['firstname']			= $user_profile['first_name'];
				$save['lastname']			= $user_profile['last_name'];
				$save['email']				= $user_profile['email'];
				$save['phone']				= '';
				$save['company']			= '';
				$save['active']				= 1;
				$save['email_subscribe']	= 1;
				
				$save['password']			= substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 8);
	 
		        $save['social_id']			= $user_profile['id'];
		        $save['social_type']		= 'facebook';
				$save['social_username']	= $user_profile['username'];
				$save['social_location_id']	= $user_profile['location']['id'];
				$save['social_location_name']	= $user_profile['location']['name'];
				$save['social_gender']		= $user_profile['gender'];
				$save['social_locale']		= $user_profile['locale'];
		        
				
				// save the customer info and get their new id
				$id = $this->Customer_model->save($save);



				/* send an email */
				// get the email template
				$res = $this->db->where('id', '6')->get('canned_messages');
				$row = $res->row_array();
				
				// set replacement values for subject & body
				
				// {customer_name}
				$row['subject'] = str_replace('{customer_name}', $user_profile['first_name'].' '. $user_profile['last_name'], $row['subject']);
				$row['content'] = str_replace('{customer_name}', $user_profile['first_name'].' '. $user_profile['last_name'], $row['content']);
				
				// {url}
				$row['subject'] = str_replace('{url}', $this->config->item('base_url'), $row['subject']);
				$row['content'] = str_replace('{url}', $this->config->item('base_url'), $row['content']);
				
				// {site_name}
				$row['subject'] = str_replace('{site_name}', $this->config->item('company_name'), $row['subject']);
				$row['content'] = str_replace('{site_name}', $this->config->item('company_name'), $row['content']);
				
				$this->load->library('email');
				
				$config['mailtype'] = 'html';
				
				$this->email->initialize($config);
		
				$this->email->from($this->config->item('email'), $this->config->item('company_name'));
				$this->email->to($save['email']);
				$this->email->bcc($this->config->item('email'));
				$this->email->subject($row['subject']);
				$this->email->message(html_entity_decode($row['content']));
				
				$this->email->send();
				
				$this->session->set_flashdata('message', sprintf( lang('registration_thanks'), $user_profile['first_name'] ) );
				
				//lets automatically log them in
				$login = $this->Customer_model->fblogin($user_profile['id']);

				if($login){
					redirect('secure/login/');
				}else{
					redirect('/');
				}

 			}
	      } catch(FacebookApiException $e) {
	        // If the user is logged out, you can have a 
	        // user ID even though the access token is invalid.
	        // In this case, we'll get an exception, so we'll
	        // just ask the user to login again here.
			redirect('secure/my_account/');
	      }   
	    } else {

	      	redirect('secure/login/');

	    }

	}

}