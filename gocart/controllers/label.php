<?php

class Label extends Admin_Controller {

	function __construct()
	{
		parent::__construct();
		remove_ssl();
		
		if($this->auth->check_access('Orders'))
		{
			redirect($this->config->item('admin_folder').'/orders');
		}
	
	}
	
	function index()
	{
			
		$this->load->view($this->config->item('admin_folder').'/label');
	}

}