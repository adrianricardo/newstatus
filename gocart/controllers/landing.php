<?php

class Landing extends Front_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('landing_model');
		//make sure we're not always behind ssl
		remove_ssl();
	}

	function index()
	{
		$this->load->view('landing');
	}
	function soon()
	{
		$this->load->view('soon');
	}
	function sign_up(){
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->form_validation->set_rules('email', 'Email', 'required');


		if ($this->form_validation->run() === FALSE)
		{
			if ($this->input->post('ajax')){
				$response['error'] = true;
				$response['msg'] = validation_errors();
				echo json_encode($response);
			} else {
				$this->load->view('landing');
			}
		}
		else
		{	
			if ($this->input->post('ajax')){	
				$response['error'] = false;
				$response['msg'] = $this->landing_model->add_email();
				echo json_encode($response);
			}else{
				$response = $this->landing_model->add_email();
				$this->load->library('session');
				$this->session->set_flashdata('success', $response);
				redirect('landing');
			}
		}
	}

}