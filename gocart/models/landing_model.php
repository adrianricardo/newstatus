<?php
Class Landing_model extends CI_Model
{
		
	function __construct()
	{
		parent::__construct();
		
	}
	public function add_email()
	{
		
		$email = $this->input->post('email');

		$query = $this->db->get_where('signups',array('email'=>$email));
        if($query->num_rows() > 0)
        {
           # E-mail is found in the database
           return 'Thank you! Email added successfully!';
        } else{
			$updated_at = date('Y-m-d H:i:s');
			$data = array(
				'email' => $this->input->post('email'),
				'date' => $updated_at
			);
			
			if($this->db->insert('signups', $data))
			{
				return 'Thank you! Email added successfully!';
			} else{
				return "Something went wrong :( Please try again";
			}
		}
	}
}