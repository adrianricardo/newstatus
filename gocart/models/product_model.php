<?php
Class Product_model extends CI_Model
{
		
	// we will store the group discount formula here
	// and apply it to product prices as they are fetched 
	var $group_discount_formula = false;
	
	function __construct()
	{
		parent::__construct();
		
		// check for possible group discount 
		$customer = $this->session->userdata('customer');
		if(isset($customer['group_discount_formula'])) 
		{
			$this->group_discount_formula = $customer['group_discount_formula'];
		}
	}
	
	function product_autocomplete($name, $limit)
	{
		return	$this->db->like('name', $name)->get('products', $limit)->result();
	}
	
	function products($data=array(), $return_count=false)
	{
		if(empty($data))
		{
			//if nothing is provided return the whole shabang
			$this->get_all_products();
		}
		else
		{
			//grab the limit
			if(!empty($data['rows']))
			{
				$this->db->limit($data['rows']);
			}
			
			//grab the offset
			if(!empty($data['page']))
			{
				$this->db->offset($data['page']);
			}

			//are these phone items
			if(!empty($data['phone']))
			{
				$this->db->where('phone', 1);
			}
			
			//do we order by something other than category_id?
			if(!empty($data['order_by']))
			{
				//if we have an order_by then we must have a direction otherwise KABOOM
				$this->db->order_by($data['order_by'], $data['sort_order']);
			}
			
			//do we have a search submitted?
			if(!empty($data['term']))
			{
				$search	= json_decode($data['term']);
				//if we are searching dig through some basic fields
				if(!empty($search->term))
				{
					$this->db->like('name', $search->term);
					$this->db->or_like('description', $search->term);
					$this->db->or_like('excerpt', $search->term);
					$this->db->or_like('sku', $search->term);
				}
				
				if(!empty($search->category_id))
				{
					//lets do some joins to get the proper category products
					$this->db->join('category_products', 'category_products.product_id=products.id', 'right');
					$this->db->where('category_products.category_id', $search->category_id);
					$this->db->order_by('sequence', 'ASC');
				}
			}
			
			if($return_count)
			{
				return $this->db->count_all_results('products');
			}
			else
			{
				return $this->db->get('products')->result();
			}
			
		}
	}
	
	function get_all_products()
	{
		//sort by alphabetically by default
		$this->db->order_by('name', 'ASC');
		$result	= $this->db->get('products');
		//apply group discount
		$return = $result->result();
		if($this->group_discount_formula) 
		{
			foreach($return as &$product) {
				eval('$product->price=$product->price'.$this->group_discount_formula.';');
			}
		}
		return $return;
	}
	
	function get_products($category_id = false, $limit = false, $offset = false, $by=false, $sort=false)
	{
		//if we are provided a category_id, then get products according to category
		if ($category_id)
		{
			$this->db->select('category_products.*, LEAST(IFNULL(NULLIF(saleprice, 0), price), price) as sort_price', false)->from('category_products')->join('products', 'category_products.product_id=products.id')->where(array('category_id'=>$category_id, 'enabled'=>1));
			$this->db->order_by($by, $sort);
			
			$result	= $this->db->limit($limit)->offset($offset)->get()->result();

			$contents	= array();
			$count		= 0;
			foreach ($result as $product)
			{

				$contents[$count]	= $this->get_product($product->product_id);
				$count++;
			}

			return $contents;
		}
		else
		{
			//sort by alphabetically by default
			$this->db->order_by('name', 'ASC');
			$result	= $this->db->get('products');
			//apply group discount
			$return = $result->result();
			if($this->group_discount_formula) 
			{
				foreach($return as &$product) {
					eval('$product->price=$product->price'.$this->group_discount_formula.';');
				}
			}
			return $return;
		}
	}
	//Get custom phone order products
	function get_phone_orders()
	{
		
			//sort by alphabetically by default
			$this->db->order_by('name', 'ASC');
			$this->db->where('phone', '1');
			$this->db->where('enabled', '1');
			$this->db->where('quantity >', '0');
			$result	= $this->db->get('products');
			//apply group discount
			$return = $result->result();
			
			return $return;
		
	}
	function get_product_w_brands(){
		$sql = "Select id, name, brand, category,images,slug from cs_products LEFT JOIN (Select product_id, category_id, slug as brand from cs_category_products cp 
							 JOIN cs_categories c ON cp.category_id = c.id WHERE parent_id = 4
						  ) brands on id = brands.product_id 
						  LEFT JOIN (Select product_id, category_id, slug as category from cs_category_products cp 
							JOIN cs_categories c ON cp.category_id = c.id WHERE parent_id = 1
						 ) category on id = category.product_id
				WHERE id NOT IN
				(Select p.id from cs_products p  
					JOIN (Select product_id, category_id, slug as brand from cs_category_products cp 
							JOIN cs_categories c ON cp.category_id = c.id WHERE parent_id = 4
						  ) brands on p.id = brands.product_id  
					JOIN (Select product_id, category_id, slug as category from cs_category_products cp 
							JOIN cs_categories c ON cp.category_id = c.id WHERE parent_id = 1
						 ) category on p.id = category.product_id
				) order by name ";
		$query = $this->db->query($sql);
		return $query->result();
	}
	function get_brands_and_categories(){
		$sql = "Select slug, id from cs_category_products cp RIGHT JOIN cs_categories c ON cp.category_id = c.id 
				 WHERE parent_id = 1 OR parent_id = 4 GROUP BY slug ORDER BY parent_id";
		$query = $this->db->query($sql);
		return $query->result();			 	
	}
	function set_categories(){

		$products	= $this->input->post('product');
		$category	= $this->input->post('category');
		
		if(!$products)
		{
			redirect($this->config->item('admin_folder').'/products/eps1');
		}
				
		foreach($products as $id=>$product)
		{
			$product['id']	= $id;

			$this->db->insert('category_products', 
							array(
							'product_id'=>$id,
							'category_id'=>$category,
							'updated'=>date ("Y-m-d H:i:s")
							)
						);
			
		}
	}


	function get_brands(){
		//sort by alphabetically by default
		$this->db->order_by('name', 'ASC');
		$this->db->where('parent_id', '4');
		$result	= $this->db->get('categories');
		$return = $result->result();
		
		return $return;
	}
	function filter_products($categories = false, $limit = false, $offset = false, $by=false, $sort=false)
	{

		if($categories == 'sale'){
			//sort by alphabetically by default
			$result	= $this->db->where('products.saleprice >','0');
			$results['count']	= $this->db->count_all_results('products');

			$this->db->order_by($by,$sort);
			$result	= $this->db->limit($limit)->offset($offset)->where('products.saleprice >','0')->where('products.enabled','1')->get('products');
 			$results['products'] = $result->result();
			/*if($this->group_discount_formula) 
			{
				foreach($return as &$product) {
					eval('$product->price=$product->price'.$this->group_discount_formula.';');
				}
			}*/


			return $results;

		}

		//if we are provided a slug, then get products according to category
		if (!empty($categories))
		{
			$this->db->select('category_products.*, LEAST(IFNULL(NULLIF(saleprice, 0), price), price) as sort_price', false)->from('category_products')->join('products', 'category_products.product_id=products.id')->join('categories', 'category_products.category_id=categories.id');
			$n = 0;
			$where = "products.enabled = 1";
			
				
			$where .= " AND `cs_categories`.`slug` = '".$categories."' ";
			$this->db->where($where);
			$results['count']	= $this->db->get()->num_rows();



			$this->db->select('products.*, LEAST(IFNULL(NULLIF(saleprice, 0), price), price) as sort_price', false)->from('category_products')->join('products', 'category_products.product_id=products.id')->join('categories', 'category_products.category_id=categories.id');
			$n = 0;
			$where = "products.enabled = 1";
			
				
			$where .= " AND `cs_categories`.`slug` = '".$categories."' ";
				
			$this->db->where($where);
			$this->db->order_by($by,$sort);

			
			$results['products']	= $this->db->limit($limit)->offset($offset)->get()->result();
		
			return $results;
		}
		else
		{
			//sort by alphabetically by default
			
			$results['count']	= $this->db->count_all_results('products');
			$this->db->where('products.enabled','1');
			$this->db->order_by('products.hidden','ASC');
			$this->db->order_by($by,$sort);
			$result	= $this->db->limit($limit)->offset($offset)->get('products');
			//apply group discount
			$results['products'] = $result->result();
			/*if($this->group_discount_formula) 
			{
				foreach($return as &$product) {
					eval('$product->price=$product->price'.$this->group_discount_formula.';');
				}
			}*/


			return $results;
		}
	}
	function count_all_products()
	{
		return $this->db->count_all_results('products');
	}
	
	function count_products($id)
	{
		return $this->db->select('product_id')->from('category_products')->join('products', 'category_products.product_id=products.id')->where(array('category_id'=>$id, 'enabled'=>1))->count_all_results();
	}

	function get_product($id, $related=true)
	{
		$result	= $this->db->get_where('products', array('id'=>$id))->row();
		if(!$result)
		{
			return false;
		}

		$related	= json_decode($result->related_products);
		
		if(!empty($related))
		{
			//build the where
			$where = false;
			foreach($related as $r)
			{
				if(!$where)
				{
					$this->db->where('id', $r);
				}
				else
				{
					$this->db->or_where('id', $r);
				}
				$where = true;
			}
		
			$result->related_products	= $this->db->get('products')->result();
		}
		else
		{
			$result->related_products	= array();
		}
		$result->categories			= $this->get_product_categories($result->id);
	
		// group discount?
		if($this->group_discount_formula) 
		{
			eval('$result->price=$result->price'.$this->group_discount_formula.';');
		}

		return $result;
	}

	function get_product_categories($id)
	{
		return $this->db->where('product_id', $id)->join('categories', 'category_id = categories.id')->get('category_products')->result();
	}

	function get_slug($id)
	{
		return $this->db->get_where('products', array('id'=>$id))->row()->slug;
	}

	function check_slug($str, $id=false)
	{
		$this->db->select('slug');
		$this->db->from('products');
		$this->db->where('slug', $str);
		if ($id)
		{
			$this->db->where('id !=', $id);
		}
		$count = $this->db->count_all_results();

		if ($count > 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	function save($product, $options=false, $categories=false)
	{
		if ($product['id'])
		{
			$this->db->where('id', $product['id']);
			$this->db->update('products', $product);

			$id	= $product['id'];
		}
		else
		{
			$this->db->insert('products', $product);
			$id	= $this->db->insert_id();
		}

		//loop through the product options and add them to the db
		if($options !== false)
		{
			$obj =& get_instance();
			$obj->load->model('Option_model');

			// wipe the slate
			$obj->Option_model->clear_options($id);

			// save edited values
			$count = 1;
			foreach ($options as $option)
			{
				$values = $option['values'];
				unset($option['values']);
				$option['product_id'] = $id;
				$option['sequence'] = $count;

				$obj->Option_model->save_option($option, $values);
				$count++;
			}
		}
		
		if($categories !== false)
		{
			if($product['id'])
			{
				//get all the categories that the product is in
				$cats	= $this->get_product_categories($id);
				
				//generate cat_id array
				$ids	= array();
				foreach($cats as $c)
				{
					$ids[]	= $c->id;
				}

				//eliminate categories that products are no longer in
				foreach($ids as $c)
				{
					if(!in_array($c, $categories))
					{
						$this->db->delete('category_products', array('product_id'=>$id,'category_id'=>$c));
					}
				}
				
				//add products to new categories
				foreach($categories as $c)
				{
					if(!in_array($c, $ids))
					{
						$this->db->insert('category_products', array('product_id'=>$id,'category_id'=>$c));
					}
				}
			}
			else
			{
				//new product add them all
				foreach($categories as $c)
				{
					$this->db->insert('category_products', array('product_id'=>$id,'category_id'=>$c));
				}
			}
		}
		
		//return the product id
		return $id;
	}
	
	function delete_product($id)
	{
		// delete product 
		$this->db->where('id', $id);
		$this->db->delete('products');

		//delete references in the product to category table
		$this->db->where('product_id', $id);
		$this->db->delete('category_products');
		
		// delete coupon reference
		$this->db->where('product_id', $id);
		$this->db->delete('coupons_products');

	}

	function add_product_to_category($product_id, $optionlist_id, $sequence)
	{
		$this->db->insert('product_categories', array('product_id'=>$product_id, 'category_id'=>$category_id, 'sequence'=>$sequence));
	}

	function search_products($term, $limit=false, $offset=false, $by=false, $sort=false)
	{
		$results		= array();
		
		$this->db->select('*, LEAST(IFNULL(NULLIF(saleprice, 0), price), price) as sort_price', false);
		//this one counts the total number for our pagination
		$this->db->where('enabled', 1);
		$this->db->where('(name LIKE "%'.$term.'%" OR description LIKE "%'.$term.'%" OR excerpt LIKE "%'.$term.'%" OR sku LIKE "%'.$term.'%")');
		$results['count']	= $this->db->count_all_results('products');


		$this->db->select('*, LEAST(IFNULL(NULLIF(saleprice, 0), price), price) as sort_price', false);
		//this one gets just the ones we need.
		$this->db->where('enabled', 1);
		$this->db->where('(name LIKE "%'.$term.'%" OR description LIKE "%'.$term.'%" OR excerpt LIKE "%'.$term.'%" OR sku LIKE "%'.$term.'%")');
		
		if($by && $sort)
		{
			$this->db->order_by($by, $sort);
		}
		
		$results['products']	= $this->db->get('products', $limit, $offset)->result();
		
		return $results;
	}

	// Build a cart-ready product array
	function get_cart_ready_product($id, $quantity=false)
	{
		$product	= $this->db->get_where('products', array('id'=>$id))->row();
		
		//unset some of the additional fields we don't need to keep
		if(!$product)
		{
			return false;
		}
		
		$product->base_price	= $product->price;
		
		if ($product->saleprice != 0.00)
		{ 
			$product->price	= $product->saleprice;
		}
		
		
		// Some products have n/a quantity, such as downloadables
		//overwrite quantity of the product with quantity requested
		if (!$quantity || $quantity <= 0 || $product->fixed_quantity==1)
		{
			$product->quantity = 1;
		}
		else
		{
			$product->quantity = $quantity;
		}
		
		
		// attach list of associated downloadables
		$product->file_list	= $this->Digital_Product_model->get_associations_by_product($id);
		
		return (array)$product;
	}
	function log_product_view($product_id,$customer_id){
		$this->db->insert('product_views', array('product_id'=>$product_id, 'customer_id'=>$customer_id, 'time'=>date ("Y-m-d H:i:s")));
	}
}