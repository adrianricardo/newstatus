<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Stripe_pay
{
	var $CI;
	
	//this can be used in several places
	var	$method_name;
	function __construct()
	{
		$this->CI =& get_instance();
		$this->method_name	= "Stripe_pay";
		require_once("Stripe_lib.php");
	}
	
	/*
	checkout_form()
	this function returns an array, the first part being the name of the payment type
	that will show up beside the radio button the next value will be the actual form if there is no form, then it should equal false
	there is also the posibility that this payment method is not approved for this purchase. in that case, it should return a blank array 
	*/
	//these are the front end form and check functions
	function checkout_form($post = false)
	{
		$settings	= $this->CI->Settings_model->get_settings('Stripe_pay');
		$enabled	= $settings['enabled'];
		
		$form			= array();
		$form['name']	= $this->method_name;

		// Retrieve any previously stored cc data to redisplay in case of errors
		$token_data = $this->CI->session->userdata('stripeToken');

		if($enabled)
		{
			
			$form['form'] = $this->CI->load->view('customer_card',array('stripeToken'=>$token_data,'stripeKey'=>$this->CI->config->item("STRIPE_API_PUB")),true);
			
			return $form;
			
		} else return array();
	}
	function checkout_check()
	{
		$token_data = $this->CI->session->userdata('stripeToken');

		//if the token is not present and the the stored token is not present return error
		if(!isset($_POST['stripeToken']) && !($token_data)){  
			return "Please enter billing information";
		}else 
		{

			//if the post token is present store it
			if($_POST['stripeToken']){
				
				//set api key
				Stripe::setApiKey($this->CI->config->item('STRIPE_API'));

				try {
					$customer = Stripe_Customer::create(array(
				  		'card'  => $_POST['stripeToken']
					));
					$newdata = array(
		                   'address1'  => $customer->active_card->address_line1,
		                   'address2'     => $customer->active_card->address_line2,
		                   'city' => $customer->active_card->address_city,
		                   'zone' => $customer->active_card->address_state,
		                   'zip' => $customer->active_card->address_zip,
		                   'name' => $customer->active_card->name,
		                   'country' => $customer->active_card->address_country,
		                   'type' => $customer->active_card->type,
		                   'last4' => $customer->active_card->last4,
		                   'exp_month' => $customer->active_card->exp_month,
		                   'exp_year'  => $customer->active_card->exp_year,
		                   'stripeToken' => $_POST['stripeToken'],
		                   'customerID' => $customer->id
		            );

					$this->CI->session->set_userdata($newdata);


				} catch(Stripe_CardError $e) {
					// The card has been declined, send back with error
					return "Your card has been declined";
				}
			}

			return false;
		}
	}
	
	function description()
	{
		//create a description from the session which we can store in the database
		//this will be added to the database upon order confirmation
		
		/*
		access the payment information with the  $_POST variable since this is called
		from the same place as the checkout_check above.
		*/
		
		$description	= 'Card Type: '.$this->CI->session->userdata('type').'<br />
		Name on Card: '.$this->CI->session->userdata('name').'<br />
		Card Number: ***********'.$this->CI->session->userdata('last4').'<br />
		Expires: '.$this->CI->session->userdata('exp_month').'/'.$this->CI->session->userdata('exp_year');

		return $description;
			
	}
	
	//back end installation functions
	function install()
	{
		//set a default blank setting for flatrate shipping
		$this->CI->Settings_model->save_settings('Stripe_pay', array('enabled'=>'0'));
	}
	
	function uninstall()
	{
		$this->CI->Settings_model->delete_settings('Stripe_pay');
	}
	
	//payment processor
	function process_payment($email,$amount)
	{

		Stripe::setApiKey($this->CI->config->item('STRIPE_API'));

		$id = $this->CI->session->userdata('customerID');

		try {
			//update customer object with email
			$cu = Stripe_Customer::retrieve($id);
			$cu->email = $email;
			$cu->save();
			//charge customer
			$charge = Stripe_Charge::create(array(
				'customer' => $id,
				'amount'   => $amount,
			  	'currency' => 'usd',
			  	'description' => $email
			));


		} catch(Stripe_CardError $e) {
			// The card has been declined, send back with error
			return "Your card has been declined";
		}

		return false; //no error
	}
	function clear_session(){
		$olddata = array(
                   'address1'  	=> '',
                   'address2'  	=> '',
                   'city'		=> '',
                   'zone'		=> '',
                   'zip' 		=> '',
                   'name' 		=> '',
                   'country' 	=> '',
                   'stripeToken' => '',
                   'last4' 		=> '',
                   'exp_month'	=> '',
                   'exp_year'	=> ''
        );
        $this->CI->session->unset_userdata($olddata);
	}
	
	//admin end form and check functions
	function form($post	= false)
	//GOOD!
	{
		//this same function processes the form
		if(!$post)
		{
			$settings	= $this->CI->Settings_model->get_settings('Stripe_pay');
			$enabled	= $settings['enabled'];
		}
		else
		{
			$enabled	= $post['enabled'];
		}
		
		ob_start();
		?>

		<label><?php echo lang('enabled');?></label>
		<select name="enabled" class="span3">
			<option value="1"<?php echo((bool)$settings['enabled'])?' selected="selected"':'';?>><?php echo lang('enabled');?></option>
			<option value="0"<?php echo((bool)$settings['enabled'])?'':' selected="selected"';?>><?php echo lang('disabled');?></option>
		</select>
		<?php
		$form =ob_get_contents();
		ob_end_clean();
		
		return $form;
	}
	
	function check()
	//GOOD!
	{	
		$error	= false;
		
		//there is no need for error checking on this form, but this is how it will generally be done.
		//test against $_POST 
		
		//count the errors
		if($error)
		{
			return $error;
		}
		else
		{
			//we save the settings if it gets here
			$this->CI->Settings_model->save_settings('Stripe_pay', array('enabled'=>$_POST['enabled']));
			
			return false;
		}
	}
}