<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

  <script src="https://checkout.stripe.com/v2/checkout.js"></script>
  <button id="new-card" class="btn btn-gold btn-big">Enter new card </button>

  <script>
    $('#new-card').click(function(){
      var token = function(res){
        var $input = $('<input type=hidden name=stripeToken />').val(res.id);
        $('form').append($input).submit();
      };

      StripeCheckout.open({
        key:         '<?php echo $stripeKey; ?>',
        address:     true,
        amount:      <?php echo $this->go_cart->total()*100; ?>,
        currency:    'usd',
        name:        'Collective Status',
        description: 'You can confirm your order on the next page',
        panelLabel:  'Continue',
        token:       token
      });

      return false;
    });
  </script>