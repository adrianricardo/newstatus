csjo = {
    userIsLogined: false,
    base_url: window.location.protocol + "//" + window.location.host + "/newstatus/",
    History: '',
    count: 0,
    doneFlag: true,
    loadDelay: false,
    manualStateChange: false,
    opts: {
            lines: 7, // The number of lines to draw
            length: 10, // The length of each line
            width: 6, // The line thickness
            radius: 10, // The radius of the inner circle
            corners: 1, // Corner roundness (0..1)
            rotate: 0, // The rotation offset
            direction: 1, // 1: clockwise, -1: counterclockwise
            color: '#FFF', // #rgb or #rrggbb
            speed: 1.4, // Rounds per second
            trail: 0, // Afterglow percentage
            shadow: false, // Whether to render a shadow
            hwaccel: false, // Whether to use hardware acceleration
            className: 'spinner', // The CSS class to assign to the spinner
            zIndex: 2e9, // The z-index (defaults to 2000000000)
            top: 'auto', // Top position relative to parent in px
            left: 'auto' // Left position relative to parent in px
          },
    init: function () {
        csjo.initShade();
        csjo.initResize();
        csjo.initScrolling();
    },

    initNav: function (user,url) {     
        csjo.filter = "";
        var search = $('#search');

        //history API
        (function(window,undefined){

            // Bind to StateChange Event
            History.Adapter.bind(window,'statechange',function(){ // Note: We are using statechange instead of popstate
                var State = History.getState(); // Note: We are using History.getState() instead of event.state
            });

        })(window);
        History.replaceState({state:''}, "All", "");
        var states =  History.getState();
        
        //states = states.data.state,

        window.onstatechange = function(){
            //clear position on slider
            //get new position
            var obj = History.getState();
            var step = obj.data.state;
 
            //reset count
            csjo.count = 0;

            if (csjo.manualStateChange == false){
                csjo.UpdateFilters(step);
                csjo.getProducts(step,false,csjo.count);
                
            }
            //updatePosition(step,prevStep,obj.title);
            
            csjo.manualStateChange = false;
        };

        $('#phone').on('click',function(e){
            e.preventDefault();
            window.location = csjo.base_url + "phone";
            return false;
        })

        $('#filter-options').on('click','li',function(){
            //remove mobile overlay
            $('body').removeClass('filter');
            csjo.doneFlag = false;
            csjo.loadDelay = true;
            $("html, body").animate({ scrollTop: 0 }, 400);
            window.setTimeout(  
                function() {
                    csjo.loadDelay = false;
                },  
                500  
            );  

            //is product container present?
            if($('#home').length || !$('#product-container').length){
                var prep = '<div id="sheet"></div><ul id="product-container"></ul>';
                $('#main-container').html(prep);
            }

            //show loading box
            $('#product-container').addClass('loading').html('<div id="message"><span id="loader"></span><h6>searching<span>.</span></h6></div>');
            var target = document.getElementById('loader');
            csjo.spinner = new Spinner(csjo.opts).spin(target);

            //is this a brand or category?
            var type = this.getAttribute('data-type');
            csjo.filter = this.getAttribute('data-'+type);


            if($(this).hasClass('active')){
                $(this).removeClass('active');
                $('#'+type+'-filter').removeClass('active');
                csjo.filter = "";
            }else{

                //mark this li element
                $('#filter-options li').removeClass('active')
                $(this).addClass('active');


                //highlight 
                if(type == 'category'){
                    $('#filter-categories li').removeClass('active');
                    $('#category-filter').addClass('active');
                }else if(type == 'brand'){
                    $('#filter-categories li').removeClass('active');
                    $('#brand-filter').addClass('active');
                 }

            }
            //reset count
            csjo.count = 0;
            
            csjo.getProducts(csjo.filter,false,csjo.count);

            var obj = History.getState();
            csjo.manualStateChange = true;
            History.pushState({state:csjo.filter},csjo.filter, "?category="+csjo.filter);
            rootUrl = History.getRootUrl(),
            State = History.getState(),
            url = State.url;
            relativeUrl = url.replace(rootUrl,'');
            _gaq.push(['_trackPageview', relativeUrl]);

         });
        
        $(document).on('click','#category-filter',function(){
            $('.panel').hide();
            $('#category-panel').show();
            $('#brand-filter').removeClass('selected');
            $(this).addClass('selected');
        })
        $(document).on('click','#brand-filter',function(){
            $('.panel').hide();
            $('#brand-panel').show();
            $('#category-filter').removeClass('selected');
            $(this).addClass('selected');
        })

        $('body').on('mouseover','.trigger',function(){

            var img = $(this)[0].getAttribute('data-image');
            $(this).parent().next('img').attr('src',csjo.base_url+'uploads/images/small/'+img);

        });
        $('body').on('mouseout','.trigger',function(){
            var img = $(this).parent()[0].getAttribute('data-image-original');
            $(this).parent().next('img').attr('src',csjo.base_url+'uploads/images/small/'+img);
        });
        $('#search').on('click','.filter-dropdown', function(){
            $(this).toggleClass('active');
        })
        $('body').on('click', function(event){
            if($(event.target).hasClass('filter-dropdown')){
                $('.filter-dropdown').removeClass('active');
            }
            $(this).removeClass('active');
        })

        //new filter
        $('#new-filter').on('click',function(){
            $('#filter-categories li').removeClass('active');
            $(this).addClass('active');
            doneFlag = false;

            //is product container present?
            if($('#home').length || !$('#product-container').length){
                var prep = '<div id="sheet"></div><ul id="product-container"></ul>';
                $('#main-container').html(prep);
            }

            $('#product-container').addClass('loading').html('<div id="message"><span id="loader"></span><h6>searching<span>.</span></h6></div>');
            var target = document.getElementById('loader');
            csjo.spinner = new Spinner(csjo.opts).spin(target);
            csjo.filter = "";
            csjo.getProducts(csjo.filter,false,0);
            csjo.loadDelay = true;
            window.setTimeout(  
                function() {
                    csjo.loadDelay = false;
                },  
                500  
            );
        })

        //sale filter
        $('#sale-filter').on('click',function(){
            
            if($(this).hasClass('active')){
                $('#filter-categories li').removeClass('active');
                $('#new-filter').addClass('active');
                csjo.filter = "";
                $(this).removeClass('active');
            }
            else{
                $('#filter-categories li').removeClass('active');
                csjo.filter = "sale";
                $(this).addClass('active');
            }
            
            
            doneFlag = false;

            //is product container present?
            if($('#home').length || !$('#product-container').length){
                var prep = '<div id="sheet"></div><ul id="product-container"></ul>';
                $('#main-container').html(prep);
            }

            $('#product-container').addClass('loading').html('<div id="message"><span id="loader"></span><h6>searching<span>.</span></h6></div>');
            var target = document.getElementById('loader');
            csjo.spinner = new Spinner(csjo.opts).spin(target);
            csjo.getProducts(csjo.filter,false,0);
            csjo.loadDelay = true;
            window.setTimeout(  
                function() {
                    csjo.loadDelay = false;
                },  
                500  
            );
        })

        //spacers
        csjo.spacer = ' <li class="product empty emptyA"></li> ';
        csjo.spacer += ' <li class="product empty emptyB"></li> ';
        csjo.spacer += ' <li class="product empty emptyC"></li> ';
        csjo.spacer += ' <li class="product empty emptyD"></li> ';
        csjo.spacer += ' <li class="product empty emptyE"></li> ';
        csjo.spacer += '<span class="stretch"></span>';

        //see if they have already seen the "shop help"
        csjo.checkCookie();

  

        //mobile overlay
        $('#filter-trigger,#shop-help').on('click',function(e){
            e.preventDefault;
            $('#shop-help').hide();
            $('body').addClass('filter');
            csjo.setCookie('shop_help','seen',30);

        })
        $('#filter-close').on('click',function(e){
            e.preventDefault;
            $('body').removeClass('filter');
        })

        $('#new-card').on('click',function(){
            $('#searchform').remove();
        })

        //handle search
        $('#searchform').on('submit',function(e){
            e.preventDefault();
             //reset offset
            csjo.count = 0;
            $('#category-panel li').removeClass('active');
            $('#filter-categories li').removeClass('active');
            $('#brand-panel li').removeClass('active');
            if($('#home').length || !$('#product-container').length){
                var prep = '<div id="sheet"></div><ul id="product-container"></ul>';
                $('#main-container').html(prep);
            }
            csjo.term = $('#search-input').val();
            $('#product-container').addClass('loading').append('<div id="message"><h6>searching<span>.</span></h6></div>');
            csjo.searchProducts(csjo.term,false,csjo.count*20);
        })

        $('.facebookLogin').on('click',function(){
             FB.login(function(response) {
               window.location = csjo.base_url+"ajax/facebookLogin";
             }, {scope: 'email,user_likes'});
        })

        $('#view-more-brands').on('click',function(){
            $('#filter-options').toggleClass('open');
        })

    }, 
    getUrlVars: function(){
        var vars = [], hash;
        var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
        for(var i = 0; i < hashes.length; i++)
        {
            hash = hashes[i].split('=');
            vars.push(hash[0]);
            vars[hash[0]] = hash[1];
        }
        return vars;
    },
    initHome: function() {
        doneFlag = false;
        
        var category = csjo.getUrlVars()["category"];
        var obj = History.getState();
        var title = obj.data.title;
        if (typeof(category) != "undefined"){
            $('#home').replaceWith('<div id="product-container"></div>');
            $('#banner-space').remove();
            $('#product-container').addClass('loading').append('<div id="message"><span id="loader"></span><h6>searching<span>.</span></h6></div>');
            var target = document.getElementById('loader');
            csjo.spinner = new Spinner(csjo.opts).spin(target);
            History.replaceState({state:category}, category, "");
            csjo.UpdateFilters(category);
            csjo.filter = category;
            csjo.getProducts(csjo.filter,false,csjo.count);
            csjo.loadDelay = true;
            window.setTimeout(  
                function() {
                    csjo.loadDelay = false;
                },  
                500  
            );
        }else {
            $('#product-container').addClass('loading').append('<div id="message"><span id="loader"></span><h6>searching<span>.</span></h6></div>');
            var target = document.getElementById('loader');
            csjo.spinner = new Spinner(csjo.opts).spin(target);
            csjo.filter = "";
            csjo.getProducts(csjo.filter,false,csjo.count);
            csjo.loadDelay = true;
            window.setTimeout(  
                function() {
                    csjo.loadDelay = false;
                },  
                500  
            );
        }
        //    csjo.filter = "";
        //}

        $('')
        
    },
    initShade: function () {
        $(window).scroll(function(d,h) {
            if(Modernizr.touch){
                return false;
            }
            $products = $('#sheet');
            a = $('#main-container').offset().top + 606;
            b = $(window).scrollTop() + $(window).height();
            c = b + 30;
            $products.css('height',$(window).scrollTop() - 25);
        });
    },
    getProducts: function(filter, append, offset){
        csjo.doneFlag = true;
        //$('#product-container').addClass('loading');
        $.ajax({
            type: 'POST',
            url: csjo.base_url+'ajax',
            data: '&category='+filter+'&offset='+offset,
            success:function(data){
            // success! add item to category table
                //if this takes more than 5 seconds, ask them to refresh
                t = setTimeout(  
                    function() {
                        $('#product-container').html('<div id="message"><h6>ERROR<span>.</span><a href="'+document.URL+'">Click to refresh this page</a></h6></div>');
                    },  
                    10000  
                );
              var json = jQuery.parseJSON(data);
              if(append == true){
                var html = '';
              }else{
                var html = '<div id="sheet"></div>';
              }
              if (json.products.length == 0){
                if(append == true){
                    var html = '<h6>The end<span>.</span></h6>';
                    $('#product-container').append(csjo.spacer);
                    $('#product-container').removeClass('loading');
                    $('#message').html(html);
                    clearTimeout(t);
                }else{
                    $('#product-container').removeClass('loading').html('<div id="message"><h6>No results<span>.</span></h6> try removing some filters</div>');
                    clearTimeout(t);
                    $('#product-count').html(0)
                }
              } else {
                $.each(json.products, function() {
                    var photo = '../img/no_picture.png';
                    var imgObj = jQuery.parseJSON(this.images);
                    var additional_images = []
                    var n = 0;
                    $.each(imgObj, function(){
                        if(n<4){
                            if(n==0){   
                                if (this.filename != undefined) {
                                    photo   = '/uploads/images/small/'+this.filename;
                                    original = this.filename;
                                    n++;
                                }else{
                                    photo   = "<php echo theme_img('no_picture.png'); ?>";
                                };

                            }else{
                                if (this.filename != undefined){
                                    additional_images.push(this.filename);
                                }
                                n++;
                            }
                        }
                    });
                    html += ' <li class="product wave-'+csjo.count+'">';
                    html += '<div class="info">';
                    html += '<a href="'+ csjo.base_url + this.slug + '"><span class="view">VIEW</span></a>';
                    if(this.saleprice > 0){
                        html +=     '<span class="price-slash">$'+Math.round(this.price)+'</span>';
                        html +=     '<span class="price-reg">$'+Math.round(this.saleprice)+'</span>';
                    }else{
                        html +=     '<span class="price-reg">$'+Math.round(this.price)+'</span>';
                    }
                    html +=     '<div class="social">';
                    html +=         '<a class="tweet" href="javascript:csjo.Popup(\'http://twitter.com/share?text=' + encodeURIComponent(this.name) + '&lang=pt&url=' + encodeURIComponent('http://www.collectivestatus.com/' + this.slug) + '\')" target="_blank">TWEET</a>';
                    html +=         ' <a class="share" href="javascript:csjo.Popup(\'https://www.facebook.com/sharer/sharer.php?u=' + encodeURIComponent('http://www.collectivestatus.com/' + this.slug) + '\')" target="_blank">SHARE</span>';
                    html +=     '</div>';
                    html += '</div>';
                    html += '<a class="thumbnail" href="'+csjo.base_url+this.slug+'">';
                    html += '<div class="triggers col' + eval(n-1) + '" data-image-original="'+original+'">';
                                    if (additional_images.length){
                                        $.each(additional_images, function(){
                                            html += '<span class="trigger" data-image="'+this+'"></span>'
                                        })
                                    }
                    html +=     '</div>';      
                    html +=     '<img src="'+csjo.base_url +photo+'" />';   
                    if(this.track_stock == 1 && this.quantity < 1){
                          html +=  '<div class="stock_msg">SOLD OUT</div>';
                    }     
                    html += '</a>';
                    html += '<h5><a href="'+csjo.base_url+this.slug+'">'+this.name+'</a></h5>';
                    html += '</li> ';
                });
                csjo.spinner.stop();
                $('#product-count').html(json.count);
                if(append == true){
                    $('li.empty').remove();
                    $('.stretch').remove();
                    $('#message').remove();
                    $('#product-container').removeClass('loading').append(html + csjo.spacer);
                    $('.wave-'+csjo.count);
                 }else{
                    $('#product-container').removeClass('loading').html(html + csjo.spacer);
                }
                clearTimeout(t);
                csjo.count++;
                //enable loading again if not empty
                if (json.products.length){
                    csjo.doneFlag = false;
                }
              }
            },
            error:function(){
                // failed request; give feedback to user
                //ERRRORR HERRRERER
                $('#status-panel').html('<div class="alert alert-block alert-error fade in"><button type="button" class="close" data-dismiss="alert">×</button>Whoops. Please try again.</div>');
                $('#product-count').val('0');
            }
        });
    },
    searchProducts: function(term, append, offset){
        csjo.doneFlag = true;
        csjo.filter = true;
        $('#product-container').addClass('loading');
        $.ajax({
            type: 'POST',
            url: csjo.base_url+'ajax/search',
            data: '&term='+term+'&offset='+offset,
            success:function(data){
            // success! add item to category table
              var json = jQuery.parseJSON(data);
               if(append == true){
                var html = '';
              }else{
                var html = '<div id="sheet"></div>';
              }
              if (json.products.length == 0){
                if(append == true){
                    var html = '<h6>The end<span>.</span></h6>';
                    $('#product-container').append(csjo.spacer);
                    $('#product-container').removeClass('loading');
                    $('#message').html(html);
                }else{
                    $('#product-container').removeClass('loading').html('<div id="message"><h6>No results<span>.</span></h6> try a different search</div>');
                    $('#product-count').html(0)
                }
              } else {
                $.each(json.products, function() {
                    var photo = '../img/no_picture.png';
                    var imgObj = jQuery.parseJSON(this.images);
                    var additional_images = []
                    var n = 0;
                    $.each(imgObj, function(){
                        if(n<4){
                            if(n==0){   
                                if (this.filename != undefined) {
                                    photo   = '/uploads/images/small/'+this.filename;
                                    original = this.filename;
                                    n++;
                                }else{
                                    photo   = "<php echo theme_img('no_picture.png'); ?>";
                                };

                            }else{
                                if (this.filename != undefined){
                                    additional_images.push(this.filename);
                                }
                                n++;
                            }
                        }
                    });
                    html += ' <li class="product wave-'+csjo.count+'">';
                    html += '<div class="info">';
                    html += '<a href="'+ csjo.base_url + this.slug + '"><span class="view">VIEW</span></a>';
                    if(this.saleprice > 0){
                        html +=     '<span class="price-slash">$'+Math.round(this.price)+'</span>';
                        html +=     '<span class="price-reg">$'+Math.round(this.saleprice)+'</span>';
                    }else{
                        html +=     '<span class="price-reg">$'+Math.round(this.price)+'</span>';
                    }
                    html +=     '<div class="social">';
                    html +=         '<a class="tweet" href="http://twitter.com/share?text=' + encodeURIComponent(this.name) + '&lang=pt&url=' + encodeURIComponent('http://www.collectivestatus.com/' + this.slug) + '" target="_blank">TWEET</a>';
                    html +=         ' <a class="share" href="https://www.facebook.com/sharer/sharer.php?u=' + encodeURIComponent('http://www.collectivestatus.com/' + this.slug) + '" target="_blank">SHARE</span>';
                    html +=     '</div>';
                    html += '</div>';
                    html += '<a class="thumbnail" href="'+csjo.base_url+this.slug+'">';
                    html += '<div class="triggers col' + eval(n-1) + '" data-image-original="'+original+'">';
                                    if (additional_images.length){
                                        $.each(additional_images, function(){
                                            html += '<span class="trigger" data-image="'+this+'"></span>'
                                        })
                                    }
                    html +=     '</div>';      
                    html +=     '<img src="'+csjo.base_url +photo+'" />';    
                    if(this.track_stock == 1 && this.quantity < 1){
                          html +=  '<div class="stock_msg">OUT OF STOCK</div>';
                    }    
                    html += '</a>';
                    html += '<h5><a href="'+csjo.base_url+this.slug+'">'+this.name+'</a></h5>';
                    html += '</li> ';
                });
                $('#product-count').html(json.count);
                if(append == true){
                    $('li.empty').remove();
                    $('.stretch').remove();
                    $('#message').remove();
                    $('#product-container').removeClass('loading').append(html + csjo.spacer);
                    $('.wave-'+csjo.count);
                }else{
                    $('#product-container').removeClass('loading').html(html + csjo.spacer);
                }
                csjo.count++;
                //enable loading again if not empty
                if (json.products.length){
                    csjo.doneFlag = false;
                }
              }
            },
            error:function(){
                // failed request; give feedback to user
                //ERRRORR HERRRERER
                $('#product-container').removeClass('loading').html('<div id="message"><h6>Error<span>.</span></h6> try again?!</div>');                
                $('#product-count').val('0');
            }
        });
    },
    initScrolling: function(){
        $(window).scroll(function(){  
             if(csjo.loadDelay){
                return false;
                console.log('1');
            }
            var sTop = $(window).scrollTop();
            if(Modernizr.touch){
                sTop = sTop + 59;
                console.log('2');
            }
            if  (sTop >= $(document).height() - $(window).height() && csjo.doneFlag == false){  
                $('#product-container').append('<div id="message"><h6>searching<span>.</span></h6></div>');
                if(csjo.filter != true){
                    console.log('5');
                    csjo.getProducts(csjo.filter,true,csjo.count*20);
                }else{
                    csjo.searchProducts(csjo.term,true,csjo.count*20);
                }
                csjo.loadDelay = true;
                window.setTimeout(  
                    function() {
                        csjo.loadDelay = false;
                        console.log('6');
                    },  
                    500  
                );
                console.log('7');
            }  
        });
    },
    initProduct: function(){
        $options = $('button.option-box');
        $('#option-container').on('click','button.option-box',function(){
            //remove error
            $('#add-to-cart-status').html('').removeClass('error');
            $options.removeClass('selected');
            $(this).addClass('selected');
            var id = this.getAttribute('data-option-id');
            var value = this.getAttribute('data-value');
            $('#option-'+id).val(value);
        })
        $('#product-page').on('click','#add-to-cart',function(e){
            e.preventDefault();
            //if product has required option check if selected
            var optionID = this.getAttribute('data-required');
            if (optionID != 'false'){
                if($('#'+optionID).val() == ''){
                    $('#add-to-cart-status').html('Please select a size').addClass('error');
                    return false;
                }else{
                    $('#buy-product').submit();
                }
            }else{
                $('#buy-product').submit();
            }
        })
        $('#product-page').on('mouseover','.product-images img',function(){
            $('#primary-img img:first').attr('src',$(this).attr('src'));
        })
    },
    initCart: function(){
        //show update button on qty change
        $('#cart td').on('keyup','input.quantity',function(){
            $(this).next('.update').css('display','block');
        })
    },
    initResize: function(){
        $(document).ready(function(){
            resize();
        })
        $(window).resize(function() {
            resize();
        });
        function resize(){
            if ($(window).width() < 769){
                $('#category-panel').css('width','');
                $('#brand-panel').css('width','');
                return false;
            };
            var screenWidth = $(window).width();
            var ContainerPadding = (parseFloat($('.container').css('padding-left'))*2);
            var Categories = ($('#filter-categories').width())+14;
            var filterPadding = (parseFloat($('#filters').css('padding-left'))*2);
            var total = ContainerPadding+Categories+filterPadding;
            $('#category-panel').css('width',screenWidth-total);
            $('#brand-panel').css('width',screenWidth-total);
        }
    },
    UpdateFilters: function(filter){
        //clear active  filters
        $('#filter-options li').removeClass('active');
        //clear active filter group
        $('#filter-categories li').removeClass('selected active');
        //add active class to correct filter
        $('#'+filter).addClass('active');
        
        if($('#'+filter).length){
            var cat = $('#'+filter)[0].getAttribute('data-type');
            //add active and selected class to filter group
            $('#'+cat+'-filter').addClass('selected active');
            //hide respective filter panel
            $('#filter-options ul').hide();
            $('#'+cat+'-panel').show();
        }
    },
    Popup: function(url){
        window.open( url, "Share", "status = 1, height = 500, width = 360, resizable = 0" )
    },
    facebookLogin: function(response){
        window.location = csjo.base_url+"ajax/facebookLogin";
    },
    setCookie: function (c_name,value,exdays){
        var exdate=new Date();
        exdate.setDate(exdate.getDate() + exdays);
        var c_value=escape(value) + ((exdays==null) ? "" : "; expires="+exdate.toUTCString());
        document.cookie=c_name + "=" + c_value;
    },
    getCookie: function (c_name){
        var c_value = document.cookie;
        var c_start = c_value.indexOf(" " + c_name + "=");
        if (c_start == -1)
          {
          c_start = c_value.indexOf(c_name + "=");
          }
        if (c_start == -1)
          {
          c_value = null;
          }
        else
          {
          c_start = c_value.indexOf("=", c_start) + 1;
          var c_end = c_value.indexOf(";", c_start);
          if (c_end == -1)
          {
        c_end = c_value.length;
        }
        c_value = unescape(c_value.substring(c_start,c_end));
        }
        return c_value;
    },
    checkCookie: function(){
        var shop_help=csjo.getCookie("shop_help");
        if (shop_help!="seen"){
          $('#shop-help').addClass('show');
        }
    }
}

$(document).ready(function(){
    csjo.init();
});