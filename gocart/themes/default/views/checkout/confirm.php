<div class="step-4 checkout">
	<h2>Confirm order</h2>
		
	<?php include('summary.php');?>

	<div class="continue-container">
		<a class="btn btn-gold btn-big" href="<?php echo site_url('checkout/place_order');?>"><?php echo lang('submit_order');?></a>
		<table class="table">
			<?php
			/**************************************************************
			Subtotal Calculations
			**************************************************************/
			?>
			<?php if($this->go_cart->group_discount() > 0)  : ?> 
        	<tr>
        		<td colspan="2">
				<td colspan="1"><strong><?php echo lang('group_discount');?></strong></td>
				<td><?php echo format_currency(0-$this->go_cart->group_discount()); ?>                </td>
			</tr>
			<?php endif; ?>
			
			<tr>
				<td colspan="2">
		    	<td colspan="1"><strong><?php echo lang('subtotal');?></strong></td>
				<td id="gc_subtotal_price"><?php echo format_currency($this->go_cart->subtotal()); ?></td>
			</tr>
			
				
			<?php if($this->go_cart->coupon_discount() > 0) {?>
		    <tr>
		    	<td colspan="2">
		    	<td colspan="1"><strong><?php echo lang('coupon_discount');?></strong></td>
				<td id="gc_coupon_discount">-<?php echo format_currency($this->go_cart->coupon_discount());?></td>
			</tr>
				<?php if($this->go_cart->order_tax() != 0) { // Only show a discount subtotal if we still have taxes to add (to show what the tax is calculated from)?> 
				<tr>
					<td colspan="2">
		    		<td colspan="1"><strong><?php echo lang('discounted_subtotal');?></strong></td>
					<td id="gc_coupon_discount"><?php echo format_currency($this->go_cart->discounted_subtotal());?></td>
				</tr>
				<?php
				}
			} 
			/**************************************************************
			 Custom charges
			**************************************************************/
			$charges = $this->go_cart->get_custom_charges();
			if(!empty($charges))
			{
				foreach($charges as $name=>$price) : ?>
					
			<tr>
				<td colspan="2">
				<td colspan="1"><strong><?php echo $name?></strong></td>
				<td><?php echo format_currency($price); ?></td>
			</tr>	
					
			<?php endforeach;
			}	


			/**************************************************************
			 Custom charges
			**************************************************************/

			$insurance = $this->go_cart->get_insurance();
			if($insurance > 0) : ?>
			<tr>
				<td colspan="2">
				<td colspan="1"><strong>USPS Insurance</strong></td>
				<td><?php echo format_currency($insurance); ?></td>
			</tr>	
					
			<?php endif; 	
			
			
			/**************************************************************
			Order Taxes
			**************************************************************/
			 // Show shipping cost if added before taxes
			if($this->config->item('tax_shipping') && $this->go_cart->shipping_cost()>0) : ?>
				<tr>
				<td colspan="2">
				<td colspan="1"><strong><?php echo lang('shipping');?></strong></td>
				<td><?php echo format_currency($this->go_cart->shipping_cost()); ?></td>
			</tr>
			<?php endif;
			if($this->go_cart->order_tax() > 0) :  ?>
		    <tr>
		    	<td colspan="2">
		    	<td colspan="1"><strong><?php echo lang('tax');?></strong></td>
				<td><?php echo format_currency($this->go_cart->order_tax());?></td>
			</tr>
			<?php endif; 
			// Show shipping cost if added after taxes
			if(!$this->config->item('tax_shipping') && $this->go_cart->shipping_cost()>0) : ?>
				<tr>
				<td colspan="2">
				<td colspan="1"><strong><?php echo lang('shipping');?></strong></td>
				<td><?php echo format_currency($this->go_cart->shipping_cost()); ?></td>
			</tr>
			<?php endif ?>
			
			<?php
			/**************************************************************
			Gift Cards
			**************************************************************/
			if($this->go_cart->gift_card_discount() > 0) : ?>
			<tr>
				<td colspan="2">
				<td colspan="1"><strong><?php echo lang('gift_card_discount');?></strong></td>
				<td>-<?php echo format_currency($this->go_cart->gift_card_discount()); ?></td>
			</tr>
			<?php endif; ?>
			
			<?php
			/**************************************************************
			Grand Total
			**************************************************************/
			?>

			
			<tr>
				<td colspan="2">
				<td colspan="1"><strong><?php echo lang('grand_total');?></strong></td>
				<td><?php echo format_currency($this->go_cart->total()); ?></td>
			</tr>
		</table>
	</div>


	<?php include('order_details.php');?>
</div>