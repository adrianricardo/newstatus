<div class="row" id="order-details">
	<?php /* delete billing block from here */ ?>

<?php if(config_item('require_shipping')):?>
	<?php if($this->go_cart->requires_shipping()):?>
		<div class="span3 shipping-info">
			<a href="<?php echo site_url('checkout/shipping_address');?>" class="btn btn-block"><?php echo lang('shipping_address_button');?></a>
			<p>
				<?php echo format_address($customer['ship_address'], true);?>
			</p>
			<p>
				<?php echo $customer['ship_address']['phone'];?><br/>
				<?php echo $customer['ship_address']['email'];?><br/>
			</p>
		</div>

		<?php
		
		if(!empty($shipping_method) && !empty($shipping_method['method'])):?>
		<div class="span3 shipping-info">
			<p><a href="<?php echo site_url('checkout/step_2');?>" class="btn btn-block"><?php echo lang('shipping_method_button');?></a></p>
			<strong><?php echo lang('shipping_method');?></strong><br/>
			<?php $label = str_replace('&lt;sup&gt;&amp;reg;&lt;/sup&gt;','', $shipping_method['method']); ?>
			<?php $label = strtolower(str_replace(' ','-',$label)); ?>
			<?php echo $label.': '.format_currency($shipping_method['price']);?>
		</div>
		<?php endif;?>
	<?php endif;?>
<?php endif;?>

<?php if(!empty($payment_method)):?>
	<div class="span3 shipping-info">
		<p><a href="<?php echo site_url('checkout/step_3');?>" class="btn btn-block"><?php echo lang('billing_method_button');?></a></p>
		<?php echo $payment_method['description'];?>
	</div>
<?php endif;?>
</div>