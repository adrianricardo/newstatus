 <div class="billing checkout">
	<h2><?php echo lang('form_checkout');?></h2>

	<?php if (validation_errors()):?>
		<div class="alert alert-error">
			<a class="close" data-dismiss="alert">×</a>
			<?php echo validation_errors();?>
		</div>
	<?php endif;?>

	<div class="continue-container <?php if (!empty($stripeToken)) echo 'existing' ?>">
		<?php foreach ($payment_methods as $method=>$info):?>
		<?php if (!empty($stripeToken)): ?>
				<a class="btn btn-gold btn-big" href='step_4'>Continue with existing card</a>
		<?php endif; ?>
		<div id="payment-<?php echo $method;?>" class="">
			<?php echo form_open('checkout/step_3', 'id="form-'.$method.'"');?>
				<input type="hidden" name="module" value="<?php echo $method;?>" />
				<?php echo $info['form'];?>
			</form>
		</div>
		<div>
		</div>
		<?php endforeach;?>
	</div>

	<?php include('order_details.php');?>
	
</div>