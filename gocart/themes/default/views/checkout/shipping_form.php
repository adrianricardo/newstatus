<div class="step-2 checkout">
	<h2><?php echo lang('form_checkout');?></h2>

	<?php if (validation_errors()):?>
		<div class="alert alert-error">
			<a class="close" data-dismiss="alert">×</a>
			<?php echo validation_errors();?>
		</div>
	<?php endif;?>

	<?php echo form_open('checkout/step_2');?>
		<div class="row" style="position:relative">
			<div class="span6">
					<h2><?php echo lang('shipping_method');?> (USPS)</h2>
					<div class="alert alert-error" id="shipping_error_box" style="display:none"></div>
					<table class="table">
						<?php
						foreach($shipping_methods as $key=>$val):
							$ship_encoded	= md5(json_encode(array($key, $val)));
						
							if($ship_encoded == $shipping_code)
							{
								$checked = true;
							}
							else
							{
								$checked = false;
							}
						?>
						<tr style="cursor:pointer">
							<td style="width:16px;">
								<?php $label = str_replace('&lt;sup&gt;&#8482;&lt;/sup&gt;','', $key); ?>
								<?php $label = strtolower(str_replace(' ','-',$label)); ?>
								<label class="radio"><?php echo form_radio('shipping_method', $ship_encoded, set_radio('shipping_method', $ship_encoded, $checked), 'id="s'.$ship_encoded.'" class="'.$label.'"');?></label>
							</td>
							<td onclick="toggle_shipping('s<?php echo $ship_encoded;?>');"><?php echo str_replace('&lt;sup&gt;&#8482;&lt;/sup&gt;','', $key);?></td>
							<td onclick="toggle_shipping('s<?php echo $ship_encoded;?>');"><strong><?php echo $val['str'];?></strong></td>
						</tr>
						<?php endforeach;?>
					</table>
			</div>
		</div>
		<div class="continue-container">
			<div class="insurance-alert">
				<?php 	$subtotal = 0;
						foreach ($cart as $item) {
							$subtotal += $item['subtotal']; 
						}
						switch (true) {
							case ($subtotal < 50):
								$insurance =  1.65;
								break;
							case ($subtotal < 100):
								$insurance =  2.05;
								break;
							case ($subtotal < 200):
								$insurance =  2.45;
								break;
							case ($subtotal < 300):
								$insurance =  4.60;
								break;
							case ($subtotal < 400):
								$insurance =  5.50;
								break;
							case ($subtotal < 500):
								$insurance =  6.40;
								break;
							case ($subtotal < 600):
								$insurance =  7.30;
								break;
							case ($subtotal > 599):
								$insurance =  10+($subtotal-600)/(100);
						}
				?>
				<input id="insurance" name="insurance" value="<?php echo $insurance; ?>" type="checkbox"> Insure this order through <a href="https://www.usps.com/ship/insurance-and-extra-services.htm">USPS</a> insurance - $<?php echo $insurance; ?> (Express Mail required)
			</div>
			<input class="btn btn-gold btn-big" type="submit" value="<?php echo lang('form_continue');?>"/>
		</div>
	</form>

	<?php include('order_details.php');?>
	
	<script type="text/javascript">
		function toggle_shipping(key)
		{
			var check = $('#'+key);
			if(!check.attr('checked') && !$('#insurance').is(':checked'))
			{
				check.attr('checked', true);
			}
		}
	</script>
	<script>
		$(document).ready(function(){
			$('#insurance').on('click', function(){
				if($(this).is(":checked")){
					if ($('.priority-mail-express-1-day').length){
						//express
						$('.priority-mail-express-1-day:first').attr('checked', true);
						if($('.priority-mail-2-day').length){
							//priority
							$(".priority-mail-2-day:first").attr("disabled", true);
						}
					}
				}else{
					$(".priority-mail-2-day:first").attr("disabled", false);
				}
			});
		})
	</script>
</div>