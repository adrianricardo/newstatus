	<div id="cart">
		<table class="table">
			<thead>
				<thead>
					<tr>
						<th style="width:65px;">Items</th>
						<th></th>
						<th style="width:8%;">Qty</th>
						<th style="width:10%;">Price</th>
					</tr>
				</thead>
			</thead>
			
			<tbody>
				<?php
				$subtotal = 0;

				foreach ($this->go_cart->contents() as $cartkey=>$product):?>
					<tr>
						<td>
								<?php $images = json_decode($product['images']);
								if(!empty($images)){
									$img = 'uploads/images/thumbnails/'.current((array)$images)->filename;
								}else{
									$img = "/assets/img/default.jpg"; 
								}?>
								
 							<img class="thumbnail" src="<?php echo site_url($img);?>" />
						</td>
						<td>
							<span class="title"><?php echo $product['name']; ?></span>
							<div class="option">
								<?php
								if(isset($product['options'])) {
									foreach ($product['options'] as $name=>$value)
									{
										if(is_array($value))
										{
											echo '<span class="gc_option_name">'.$name.':</span><br/>';
											foreach($value as $item)
												echo '- '.$item.'<br/>';
										} 
										else 
										{
											echo '<span class="gc_option_name">'.$name.':</span> '.$value;
										}
									}
								}?>
							</div>
							<button class="remove-item" type="button" onclick="if(confirm('<?php echo lang('remove_item');?>')){window.location='<?php echo site_url('cart/remove_item/'.$cartkey);?>';}">remove</button>
						</td>
						<td style="white-space:nowrap" class="quantity">
							<?php if($this->uri->segment(1) == 'cart'): ?>
								<?php if(!(bool)$product['fixed_quantity']):?>
									<div class="control-group">
										<div class="controls">
											<div class="input-append">
												<input class="quantity" style="margin:0px;" name="cartkey[<?php echo $cartkey;?>]"  value="<?php echo $product['quantity'] ?>" size="3" type="text" autocomplete="off">
												<div class="update">
													<input type="submit" value="update"> 
												</div>
											</div>
										</div>
									</div>
								<?php else:?>
									<?php echo $product['quantity'] ?>
									<input type="hidden" name="cartkey[<?php echo $cartkey;?>]" value="1"/>
								<?php endif;?>
							<?php else: ?>
								<?php echo $product['quantity'] ?>
							<?php endif;?>
						</td>
						<td class="price"><?php echo format_currency($product['price']*$product['quantity']); ?></td>
					</tr>
				<?php endforeach;?>
			</tbody>
		</table>
	</div>