	<footer class="footer">
    © Collective Status LLC 2013 <?php echo anchor('returns', 'Return Policy', 'title="Return Policy"');?> <?php echo anchor('locations-hours', 'Locations', 'title="Locations"');?> <a href="http://gocartdv.com" target="_blank">Driven by GoCart</a> <br /><a href="http://www.adriantavares.com">Design by Adrian</a>
	</footer>
</div>
<?php if($this->uri->segment(1) == "" || $this->uri->segment(1) == "shop"){
  $url = "true";
}else{
  $url = "false";
} ?>
<?php if($this->Customer_model->is_logged_in(false, false)){
  $user = "true";
  $customer = $this->go_cart->customer();
  $email = $customer['email'];
  $created = $customer['created'];
}else{
  $user = "false";
  $email = false;
  $created = false;
} ?>
<script type='text/javascript'>
  $(document).on('ready',function(){
     csjo.initNav(<?php echo $user ?>,<?php echo $url; ?>); 
     $.backstretch("<?php echo theme_img('bg.jpg');?>", {centeredY: false});
  })
</script>

<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', '<?php echo $this->config->item("GA_TRACKING_ID"); ?>']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

<script type="text/javascript">
  (function() {
    window._pa = window._pa || {};
    // _pa.orderId = "myCustomer@email.com"; // OPTIONAL: attach user email or order ID to conversions
    // _pa.revenue = "19.99"; // OPTIONAL: attach dynamic purchase values to conversions
    var pa = document.createElement('script'); pa.type = 'text/javascript'; pa.async = true;
    pa.src = ('https:' == document.location.protocol ? 'https:' : 'http:') + "//tag.perfectaudience.com/serve/51ac1c6f5adc47f2a7000012.js";
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(pa, s);
  })();
</script>
<script>(function(){var w=window;var ic=w.Intercom;if(typeof ic==="function"){ic('reattach_activator');ic('update',intercomSettings);}else{var d=document;var i=function(){i.c(arguments)};i.q=[];i.c=function(args){i.q.push(args)};w.Intercom=i;function l(){var s=d.createElement('script');s.type='text/javascript';s.async=true;s.src='https://static.intercomcdn.com/intercom.v1.js';var x=d.getElementsByTagName('script')[0];x.parentNode.insertBefore(s,x);}if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}};})()</script>

</body>
</html>