<?php include('header.php'); ?>

<div class="row login">
	<div class="form-container">
 		<h1><?php echo lang('forgot_password');?></h1>
 		<?php echo form_open('secure/forgot_password', 'class="form-horizontal"') ?>
				<fieldset>
				
 						<div class="controls">
							<input type="text" name="email" placeholder="email"/>
						</div>
  						<div class="controls">
							<input type="hidden" value="submitted" name="submitted"/>
							<input type="submit" value="<?php echo lang('reset_password');?>" name="submit" class="btn btn-gold btn-full"/>
							<a class="secondary" href="<?php echo site_url('secure/login'); ?>"><?php echo lang('return_to_login');?></a>
						</div>
 				</fieldset>
		</form>
	</div>
</div>

<?php include('footer.php'); ?>
