<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1, maximum-scale=1, user-scalable=0"/>

		<title><?php echo (!empty($seo_title)) ? $seo_title .' - ' : ''; echo $this->config->item('company_name'); ?></title>


		<?php if(isset($meta)):?>
			<?php echo $meta;?>
		<?php else:?>
		<meta name="Keywords" content="Collective Status">
		<meta name="Description" content="Clothing boutique specializing in exclusive mens urban wear">
		<?php endif;?>

		<?php echo theme_css('main.css', true);?>
		<?php //echo theme_css('font-awesome.min.css', true);?>
		<?php echo theme_css('animate.css', true);?>
		<!--[if IE]>
			<?php echo theme_css('ie.css', true);?>
		<![endif]-->
		<?php echo theme_js('modernizr-2.6.2.min.js', true);?>
		<?php echo theme_js('jquery.js', true);?>
		<script src="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/js/bootstrap.min.js"></script>
		<?php echo theme_js('jquery.history.js', true);?>
		<?php echo theme_js('squard.js', true);?>
		<?php echo theme_js('equal_heights.js', true);?>
		<?php echo theme_js('spin.min.js', true);?>
		<?php echo theme_js($this->config->item('STATUS_JS'), true);?>
		<?php echo theme_js('noclickdelay.js', true);?>
		<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery-backstretch/2.0.3/jquery.backstretch.min.js"></script>
		<!--[if lt IE 8]>
			<?php echo theme_js('lte-ie7.css', true);?>
		<![endif]-->

		<?php
		//with this I can put header data in the header instead of in the body.
		if(isset($additional_header_info))
		{
			echo $additional_header_info;
		}

		?>
		</head>