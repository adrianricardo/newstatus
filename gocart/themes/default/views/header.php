<?php include('head.php'); ?>
<?php $brands = $this->Product_model->get_brands(); ?>
<body>
	<div id="fb-root"></div>
	<script>
	  // Additional JS functions here
	  window.fbAsyncInit = function() {
	    FB.init({
	      appId      : '682397635109272', // App ID
	      channelUrl : '//WWW.collectivestatus.COM/beta/channel.php', // Channel File
	      status     : true, // check login status
	      cookie     : true, // enable cookies to allow the server to access the session
	      xfbml      : true  // parse XFBML
	    });
	  };

	  // Load the SDK asynchronously
	  (function(d){
	     var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
	     if (d.getElementById(id)) {return;}
	     js = d.createElement('script'); js.id = id; js.async = true;
	     js.src = "//connect.facebook.net/en_US/all.js";
	     ref.parentNode.insertBefore(js, ref);
	   }(document));
	</script>
	<div class="navbar navbar-fixed-top">
		<div class="navbar-inner">
			<div class="container">
				<ul class="nav">	
					<li><a class="brand button" href="<?php echo site_url();?>"><img src="<?php echo theme_img('logo.png');?>" alt="Collective Status" /></a></li>		
					<?php /*
					<li id="men"><?php echo active_anchor("men","Men",$this->uri->segment(1)); ?></li>
					<li id="women"><?php echo active_anchor("women","Women",$this->uri->segment(1)); ?></li>
					*/ ?>
					<li class="location-item"><?php echo active_anchor("locations-hours","Locations",$this->uri->segment(1)); ?></li>
					<li class="location-item hidden-phone"><?php echo active_anchor("celebrities","Celebrities",$this->uri->segment(1)); ?></li>
					<li class="location-item hidden-phone"><?php echo active_anchor("?category=sale","Sale",$this->uri->segment(1)); ?></li>
					<li id="filter-close" class="button">X</li>
				</ul>
				<ul class="nav secondary">
					<div class="secondary-inner">
						<div class="icon-inner">
							<li id="filter-trigger" class="phone">
								<a class="button" href="#shop">Shop</a>
							</li>
							<li class="menu">
								<a href="<?php echo site_url('cart/view_cart');?>">
								<?php
								if ($this->go_cart->total_items()==0)
								{
									echo '<i class="icon-cart"></i>';
								}
								else
								{
									echo '<i class="icon-cart"><span class="count">'.$this->go_cart->total_items().'</span></i>';
								}
								?>
								</a>
							</li>
						<?php if($this->Customer_model->is_logged_in(false, false)):?>
							<li class="dropdown menu">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-user loggedin"></i></a>
								<ul id="user-dropdown" class="dropdown-menu">
									<li><a href="<?php echo  site_url('secure/my_account');?>"><?php echo lang('my_account')?></a></li>
 									<li><a href="<?php echo site_url('secure/logout');?>"><?php echo lang('logout');?></a></li>
								</ul>
							</li>
						<?php else: ?>
							<li class="dropdown menu">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-user"></i></a>
								<ul id="user-dropdown" class="dropdown-menu">
									<?php echo form_open('secure/login', 'class="form-horizontal"'); ?>
										<li><input type="text" name="email" placeholder="email"/></li>
 										<li><input type="password" name="password" placeholder="password"/></li>
 										<li><input type="submit" class="btn btn-gold" value="LOGIN" /></a></li>
 										<li><a class="secondary" href="<?php echo site_url('secure/forgot_password'); ?>"><?php echo lang('forgot_password')?></a></li>
										<li><a class="secondary" href="<?php echo site_url('secure/register'); ?>"><?php echo lang('register');?></a></li>
 										<li><a class="facebookLogin" href="#"><i class="icon-facebook"></i> Login with facebook</a></li>
										<input type="hidden" value="submitted" name="submitted"/>		
									</form>
								</ul>
							</li>
						<?php endif; ?>
					</div>
						<li id="search-bar">
				  			<div class="search-box">
				  				<span id="showing">Showing <span id="product-count">6</span> items</span>
				  				<div class="input-container">
				  					<div class="input-inner">
				  						<?php echo form_open('cart/search','id="searchform"');?>
											<input type="text" name="term" id="search-input" placeholder="<?php echo lang('search');?>"/>
										</form>
 				  					</div>
				  				</div>
				  			</div>
						</li>
					</div>
				</ul>
				
				<?php /* echo form_open('cart/search', 'class="navbar-search pull-right"');?>
					<input type="text" name="term" class="search-query span2" placeholder="<?php echo lang('search');?>"/>
				</form> */ ?>
				<div id="filter-overlay">
					<div id="filters">
						<ul id="filter-categories">
							<li id="category-filter" class="filter button"><span>filter by</span> <div class="icon-container"><i class="icon-shirt"></i></div> Category</li>
							<li id="brand-filter" class="filter button"><span>filter by</span> <div class="icon-container"><i class="icon-dolphin"></i></div> Brand</li>
							<li class="button"><div class="icon-container"><?php //<i class="icon-heart"></i> ?> </div> <?php //Popular ?></li>
							<li id="new-filter" class="button active"><div class="icon-container"><i class="icon-star"></i></div> New</li>
							<li id="sale-filter" class="button"><div class="icon-container"><i>$</i></div> Sale</li>
						</ul>
						<span class="divider black"></span>
						<span class="divider"></span>
						<div id="filter-options">
							<ul id="category-panel" class="panel">
								<li id="snapbacks" data-type="category" data-category="snapbacks" class="button"><i class="icon-snapbacks"></i> <span>Hats</span></li>
								<li id="jewelry" data-type="category" data-category="jewelry" class="button"><i class="icon-chain"></i> <span>Jewelry</span></li>
								<li id="watches" data-type="category" data-category="watches" class="button"><i class="icon-watch"></i> <span>Watches</span></li>
								<li id="crewnecks" data-type="category" data-category="crewnecks" class="button"><i class="icon-crewnecks"></i> <span>Crewnecks</span></li>
								<li id="jackets" data-type="category" data-category="jackets" class="button"><i class="icon-jacket"></i> <span>Jackets</span></li>
								<li id="shirts" data-type="category" data-category="shirts" class="button"><i class="icon-shirt"></i> <span>Shirts</span></li>
								<li id="longsleeve" data-type="category" data-category="longsleeves" class="button"><i class="icon-longsleeve"></i> <span>Longsleeve</span></li>
								<li id="tank" data-type="category" data-category="tanks" class="button"><i class="icon-tank"></i> <span>Tanks</span></li>
								<li id="shorts" data-type="category" data-category="shorts" class="button"><i class="icon-shorts"></i> <span>Shorts</span></li>
								<li id="shoes" data-type="category" data-category="shoes" class="button"><i class="icon-shoe"></i> <span>Shoes</span></li>
								<li id="accessories" data-type="category" data-category="accessories" class="button"><i class="icon-access"></i> <span>Access.</span></li>
								<li id="glasses" data-type="category" data-category="glasses" class="button"><i class="icon-glasses"></i> <span>Glasses</span></li>
								<li id="phone" data-type="category" data-category="phone" class="button"><i class="icon-phone"></i> <span>Exclusives</span></li>
							</ul>
							<ul id="brand-panel" class="panel">
								<?php foreach ($brands as $brand) :?>
									<li id="<?php echo $brand->slug ?>" data-type="brand" data-brand="<?php echo $brand->slug ?>" class="button"><?php echo $brand->name ?></li>
								<?php endforeach ?>
								<div id="view-more-brands">View more</div>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="shop-help">
			<b class="notch"></b>
			Click shop to get started
			<i class="icon-remove-sign"></i>
		</div>
	</div>
	
	<div id="main-container" class="container">	
		<div id="sheet"></div>
		<?php if ($this->session->flashdata('message')):?>
			<div class="alert alert-info">
				<a href="#" class="close" data-dismiss="alert">×</a>
				<?php echo $this->session->flashdata('message');?>
			</div>
		<?php endif;?>
		
		<?php if ($this->session->flashdata('error')):?>
			<div class="alert alert-error">
				<a href="#" class="close" data-dismiss="alert">×</a>
				<?php echo $this->session->flashdata('error');?>
			</div>
		<?php endif;?>
		
		<?php if (!empty($error)):?>
			<div class="alert alert-error">
				<a href="#" class="close" data-dismiss="alert">×</a>
				<?php echo $error;?>
			</div>
		<?php endif;?>
		

<?php
/*
End header.php file
*/