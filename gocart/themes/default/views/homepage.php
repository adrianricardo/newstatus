<?php include('head.php'); ?>
<body>
	<div class="navbar navbar-fixed-top" style="height: 50%;float: left;position: static;margin-bottom: -150px;background:none">
		<div class="navbar-inner">
			<div class="container">
				<ul class="nav">	
					<li><a class="brand button" href="<?php echo site_url();?>"><img src="<?php echo theme_img('logo.png');?>" alt="Collective Status" /></a></li>
				</ul>
			</div>
		</div>
	</div>
	
	<?php if ($this->session->flashdata('message')):?>
		<div class="alert alert-info">
			<a href="#" class="close" data-dismiss="alert">×</a>
			<?php echo $this->session->flashdata('message');?>
		</div>
	<?php endif;?>
	
	<?php if ($this->session->flashdata('error')):?>
		<div class="alert alert-error">
			<a href="#" class="close" data-dismiss="alert">×</a>
			<?php echo $this->session->flashdata('error');?>
		</div>
	<?php endif;?>
	
	<?php if (!empty($error)):?>
		<div class="alert alert-error">
			<a href="#" class="close" data-dismiss="alert">×</a>
			<?php echo $error;?>
		</div>
	<?php endif;?>
		
	<ul id="home-link">
		<li><a href="shop"><i class="icon-cart"></i>SHOP</a></li>
		<li><a href="locations-hours"><i class="icon-phone"></i>CONTACT</a></li>
		<li><a href="locations-hours"><i class="icon-location"></i>LOCATIONS</a></li>
	</ul>

	<div>
		<script type="text/javascript">
		csjo.initHome();
		$('body,html').css('height','100%').css('margin-top','0');
		</script>
		<?php include('footer.php'); ?>