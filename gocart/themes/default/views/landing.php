<?php include('header.php'); ?>
	<div class="landing">
		<div id="container">
			<h2 id="artist-callout">Click below to hear from <span class="animated">RAEKWON</span></h2>
			<div id="player">
				<img class="loading" src="<?php echo theme_img('loading.gif'); ?>" />
				<div id="js-video"></div>
				<span class="exit"><i class="icon-cancel-circle"></i><br />close</span>
			</div>
			<div id="artist-container">
				<ul id="artist" class="raekwon clearfix">
					<li class="twochainz" data-video="zwixgrLOStA"></li>
					<li class="kirko" data-video="CB6VCNeXuJM"></li>
					<li class="raekwon" data-video="CjhKcorruIg"></li>
					<li class="future" data-video="56LLoafKyH0"></li>
					<li class="driicky" data-video="YQ8awFrmzvg"></li>
					<li class="stalley" data-video="-t8L3W6dUh0"></li>
					<li class="rita" data-video="j05sBF3RqG4"></li>
					<li class="schoolboyq" data-video="I_RI-KuUUd8"></li>
					<li class="chino" data-video="_Bq0RwqFYEE"></li>
					
					<li class="vintage" data-video="gLmh3_ScLGk"></li>
				</ul>
			</div>
			<div id="social">
				<div class="fb-like" data-href="https://www.facebook.com/pages/Collective-Status-Boutique/131419700311996" data-send="false" data-layout="button_count" data-width="450" data-show-faces="false" style="top:-4px;margin-right:30px;"></div>
				<a href="https://twitter.com/share" class="twitter-share-button" data-via="statusatx" data-hashtags="newcollectivestatus">Tweet</a>
				<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
				<a href="https://twitter.com/statusatx" class="twitter-follow-button" data-show-count="true" data-size="medium">Follow @statusatx</a>
				<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
			</div>
		</div>
	</div>
</body>
<script>
var artistArray = new Array();

var future = new Array();
future['class'] = "future";
future['text'] = "Future";

var kirko = new Array();
kirko['class'] = "kirko";
kirko['text'] = "Kirko&nbsp;Bangz";

var raekwon = new Array();
raekwon['class'] = "raekwon";
raekwon['text'] = "RAEKWON";

var twochainz = new Array();
twochainz['class'] = "twochainz";
twochainz['text'] = "2&nbsp;chainz";

var driicky = new Array();
driicky['class'] = "driicky";
driicky['text'] = "driicky&nbsp;graham";

var stalley = new Array();
stalley['class'] = "stalley";
stalley['text'] = "stalley";

var rita = new Array();
rita['class'] = "rita";
rita['text'] = "Rita&nbsp;Ora";

var chino = new Array();
chino['class'] = "chino";
chino['text'] = "DJ&nbsp;Baby&nbsp;Chino";

var schoolboyq = new Array();
schoolboyq['class'] = "schoolboyq";
schoolboyq['text'] = "Schoolboy&nbsp;Q";

//var killa = new Array();
//killa['class'] = "killa";
//killa['text'] = "Marcus&nbsp;Manchild";

var vintage = new Array();
vintage['class'] = "vintage";
vintage['text'] = "Corey&nbsp;Shapiro ";


artistArray.push(kirko);
artistArray.push(chino);
artistArray.push(raekwon);
artistArray.push(driicky);
//artistArray.push(killa);
artistArray.push(stalley);
artistArray.push(twochainz);
artistArray.push(future);
artistArray.push(rita);
artistArray.push(schoolboyq);
artistArray.push(vintage);


artistScroll();

function artistScroll(){
	var n = 0;
	interval = setInterval(function(){
		$('#artist-callout span').html(artistArray[n]['text']).addClass('fadeInUp');
		$('#artist').attr('class','clearfix')
		$('#artist').addClass(artistArray[n]['class']);
		n++;
		if(n==10){
			n=0;
		}
		setTimeout(function(){$('#artist-callout span').removeClass('fadeInUp');},500);
	},4000);
}
$('#artist li').on('click',function(){
	$("html, body").animate({ scrollTop: $('#player').position().top}, 600);
	clearInterval(interval);
	$('#artist').attr('class','clearfix')
	$('#artist li').removeClass('active');
	$(this).addClass('active');
	var player = $('#player');
	player.addClass('active');
	$('#player #js-video').html('<iframe width="560" height="215" src="http://www.youtube.com/embed/'+$(this)[0].getAttribute('data-video')+'?showinfo=0&rel=0&controls=0&autoplay=1" frameborder="0" allowfullscreen></iframe>"');
	setTimeout(function(){
		$('#player').addClass('play');
	},2000)

})
$('.exit').on('click', function(){
	$('#artist li').removeClass('active');
	$('#js-video').html('');
	$('#player').removeClass('active play');
	artistScroll();
})

</script>
<?php include('footer.php'); ?>