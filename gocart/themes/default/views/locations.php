<?php include('header.php'); ?>

<div class="row page locations" style="margin:20px 0 20px 0;">
	<div class="hours">
		<h4>Store Hours:</h4>
		Sunday-Thursday: 12-9
		<br />Friday and Saturday: 12-12
		<br /><h4>Address</h4>
		516 E 6th St
		Austin, TX 78701
		<br />
		<h4 style="margin-top:10px">(512) 480-0088</h4>
		<h4>info@collectivestatus.com</h4>
	</div>
	<div class="map">
		<iframe width="425" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.com/maps?f=q&source=s_q&hl=en&geocode=&q=516%2BEast%2B6th%2BStreet%2BAustin%2C%2BTX%2B78701(Collective+Status)&ie=UTF8&z=12&t=m&iwloc=addr&output=embed"></iframe>
	</div>
	<img src="<?php echo theme_img('collective-status-downstairs.jpg'); ?>" />
	<img src="<?php echo theme_img('collective-status-upstairs.jpg'); ?>" />
</div>

</form>
<?php include('footer.php'); ?>