<?php include('header.php'); ?>

<div class="row login">
	<div class="form-container">
 			<h1>Login</h1>
 			<a class="facebookLogin" href="#"><i class="icon-facebook"></i> Login with facebook</a>
 			<?php echo form_open('secure/login', 'class="form-horizontal"'); ?>
				<fieldset>
  						<div class="controls">
							<input type="text" name="email" placeholder="email"/>
						</div>
 						<div class="controls">
							<input type="password" name="password" placeholder="password"/>
						</div>
 				
 						<div class="controls">
							<label class="checkbox">
								<input name="remember" value="true" type="checkbox" />
 								 <?php echo lang('keep_me_logged_in');?>
							</label>
						</div>
 					<div class="control-group">
						<div class="controls">
							<input type="submit" value="<?php echo lang('form_login');?>" name="submit" class="btn btn-gold btn-full"/>
							<a class="secondary" href="<?php echo site_url('secure/forgot_password'); ?>"><?php echo lang('forgot_password')?></a>
							<a class="secondary" href="<?php echo site_url('secure/register'); ?>"><?php echo lang('register');?></a>
						</div>
					</div>
				</fieldset>
				
				<input type="hidden" value="<?php echo $redirect; ?>" name="redirect"/>
				<input type="hidden" value="submitted" name="submitted"/>
				
			</form>


	</div>
</div>
<?php include('footer.php'); ?>