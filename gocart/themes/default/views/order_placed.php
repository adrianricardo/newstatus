<?php include('header.php'); ?>

<div class="page-header">
	<h2><?php echo lang('order_number');?>: <?php echo $order_id;?></h2>
</div>

<?php
// content defined in canned messages
echo $download_section;
?>
<div class="order-placed">
	<div class="row">
		<div class="span4 order-info">
			<h3><?php echo lang('account_information');?></h3>
			<?php echo (!empty($customer['company']))?$customer['company'].'<br/>':'';?>
			<?php echo $customer['firstname'];?> <?php echo $customer['lastname'];?><br/>
			<?php echo $customer['email'];?> <br/>
			<?php echo $customer['phone'];?>
		</div>
		<?php
		$ship = $customer['ship_address'];
		$bill = 'temp';
		//$customer['ship_address'];
		?>
		<div class="span4 order-info">
			<h3><?php echo ($ship != $bill)?lang('shipping_information'):lang('shipping_and_billing');?></h3>
			<?php echo format_address($ship, TRUE);?><br/>
			<?php echo $ship['email'];?><br/>
			<?php echo $ship['phone'];?>
		</div>
		<?php /* if($ship != $bill):
		<div class="span4">
			<h3><?php echo lang('billing_information');?></h3>
			<?php echo format_address($bill, TRUE);?><br/>
			<?php echo $bill['email'];?><br/>
			<?php echo $bill['phone'];?>
		</div>
		<?php endif; */?>
	</div>

	<div class="row">
		<div class="span4 order-info">
			<h3><?php echo lang('additional_details');?></h3>
			<?php
			if(!empty($referral)):?><div><strong><?php echo lang('heard_about');?></strong> <?php echo $referral;?></div><?php endif;?>
			<?php if(!empty($shipping_notes)):?><div><strong><?php echo lang('shipping_instructions');?></strong> <?php echo $shipping_notes;?></div><?php endif;?>
		</div>

		<div class="span4 order-info">
			<h3 style="padding-top:10px;"><?php echo lang('shipping_method');?></h3>
			<?php echo $shipping['method']; ?>
		</div>
		
		<div class="span4 order-info">
			<h3><?php echo lang('payment_information');?></h3>
			<?php echo $payment['description']; ?>
		</div>
		
	</div>

	<div id="cart">
		<table class="table" style="margin-top:20px;">
			<thead>
				<tr>
					<th style="width:65px;">Items</th>
					<th></th>
					<th style="width:8%;">Qty</th>
					<th style="width:10%;">Price</th>
				</tr>
			</thead>
			<tbody>
			<?php
			$subtotal = 0;
			foreach ($go_cart['contents'] as $cartkey=>$product):?>
				<tr>
					<td>
					</td>
 					<td>
						<span class="title"><?php echo $product['name']; ?></span>
						<div class="option">
							<?php
							if(isset($product['options'])) {
								foreach ($product['options'] as $name=>$value)
								{
									if(is_array($value))
									{
										echo '<span class="gc_option_name">'.$name.':</span><br/>';
										foreach($value as $item)
											echo '- '.$item.'<br/>';
									} 
									else 
									{
										echo '<span class="gc_option_name">'.$name.':</span> '.$value;
									}
								}
							}?>
						</div>
					</td>
					<td style="white-space:nowrap" class="quantity">
						<?php echo $product['quantity'];?>
					</td>
					<td class="price"><?php echo format_currency($product['price']*$product['quantity']); ?></td>				</td>
				</tr>
					
			<?php endforeach; ?>
			</tbody>
		</table>
	</div>
	<div id="totals">
		<table>
			<?php if($go_cart['group_discount'] > 0)  : ?> 
			<tr>
				<td colspan="5"><strong><?php echo lang('group_discount');?></strong></td>
				<td><?php echo format_currency(0-$go_cart['group_discount']); ?></td>
			</tr>
			<?php endif; ?>

			<tr>
				<td colspan="5"><strong><?php echo lang('subtotal');?></strong></td>
				<td><?php echo format_currency($go_cart['subtotal']); ?></td>
			</tr>
			
			<?php if($go_cart['coupon_discount'] > 0)  : ?> 
			<tr>
				<td colspan="5"><strong><?php echo lang('coupon_discount');?></strong></td>
				<td><?php echo format_currency(0-$go_cart['coupon_discount']); ?></td>
			</tr>

			<?php if($go_cart['order_tax'] != 0) : // Only show a discount subtotal if we still have taxes to add (to show what the tax is calculated from) ?> 
			<tr>
				<td colspan="5"><strong><?php echo lang('discounted_subtotal');?></strong></td>
				<td><?php echo format_currency($go_cart['discounted_subtotal']); ?></td>
			</tr>
			<?php endif;

			endif; ?>
			<?php // Show shipping cost if added before taxes
			if($this->config->item('tax_shipping') && $go_cart['shipping_cost']>0) : ?>
			<tr>
				<td colspan="5"><strong><?php echo lang('shipping');?></strong></td>
				<td><?php echo format_currency($go_cart['shipping_cost']); ?></td>
			</tr>
			<?php endif ?>
			
			<?php if($go_cart['order_tax'] != 0) : ?> 
			<tr>
				<td colspan="5"><strong><?php echo lang('taxes');?></strong></td>
				<td><?php echo format_currency($go_cart['order_tax']); ?></td>
			</tr>
			<?php endif;?>
			
			<?php // Show shipping cost if added after taxes
			if(!$this->config->item('tax_shipping') && $go_cart['shipping_cost']>0) : ?>
			<tr>
				<td colspan="5"><strong><?php echo lang('shipping');?></strong></td>
				<td><?php echo format_currency($go_cart['shipping_cost']); ?></td>
			</tr>
			<?php endif;?>

			<?php // Show insurance if selected
			if($go_cart['insurance']>0) : ?>
			<tr>
				<td colspan="5"><strong>USPS Insurance</strong></td>
				<td><?php echo format_currency($go_cart['insurance']); ?></td>
			</tr>
			<?php endif;?>
			
			<?php if($go_cart['gift_card_discount'] != 0) : ?> 
			<tr>
				<td colspan="5"><strong><?php echo lang('gift_card');?></strong></td>
				<td><?php echo format_currency(0-$go_cart['gift_card_discount']); ?></td>
			</tr>
			<?php endif;?>
			<tr> 
				<td colspan="5"><strong><?php echo lang('grand_total');?></strong></td>
				<td><?php echo format_currency($go_cart['total']); ?></td>
			</tr>
		</table>
	</div>
</div>
	<footer class="footer">
		
	</footer>
</div>
<script type="text/javascript">
	csjo.initNav(true,true); 
	$.backstretch("<?php echo theme_img('bg.jpg');?>", {centeredY: false});
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', '<?php echo $this->config->item("GA_TRACKING_ID"); ?>', 'collectivestatus.com');
  ga('require', 'ecommerce', 'ecommerce.js');
  //prepare transaction
  ga('ecommerce:addTransaction', {
	'id': '<?php echo $order_id;?>',                     	// Transaction ID. Required.
	'affiliation': 'Collective Status',   					// Affiliation or store name.
	'revenue': '<?php echo $go_cart['total']?>',            // Grand Total.
	'shipping': '<?php echo $go_cart['shipping_cost']; ?>', // Shipping.
	'tax': '<?php echo $go_cart['order_tax']; ?>'           // Tax.
  });

  //add items

  <?php foreach ($go_cart['contents'] as $cartkey=>$product):?>
	  ga('ecommerce:addItem', {
		'id': '<?php echo $order_id;?>',                
		'name': '<?php echo $product["name"]; ?>',    	// Product name. Required.
		'sku': '<?php echo $product["slug"]; ?>',       // SKU/code.
		'category': 'Clothing',         				// Category or variation.
		'price': '<?php echo $product["price"]; ?>',                 // Unit price.
		'quantity': '<?php echo $product["quantity"]; ?>'                   // Quantity.
	  });	
	<?php endforeach; ?>

 


  ga('ecommerce:send');
  ga('ecommerce:clear');
  ga('send', 'pageview');



</script>
<?php if($this->Customer_model->is_logged_in(false, false)){
  $customer = $this->go_cart->customer();
  $email = $customer['email'];
  $created = $customer['created'];
}else{
   $email = false;
  $created = false;
} ?>
</body>
</html>