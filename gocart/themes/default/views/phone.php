			<div class="alert alert-block">
			  For the latest exclusives call <strong>(512) 480-0088</strong> to place custom orders
			</div>
			<ul id="package-container">
				<?php 
					foreach ($packages as $package) {
						echo '<li class="custom-package">';
							echo '<span class="span2"><div class="box"><img src="'.theme_img("logo.png").'" /></div></span>';
							echo '<span class="span8">';
								echo '<h4>'.$package->name.'</h4>';
								echo '<p>'.$package->description.'</p>';
							echo '</span>';
							echo form_open('cart/add_to_cart', 'class="form-horizontal span2" id="buy-product"');
								echo '<input type="hidden" name="cartkey" value="'.$this->session->flashdata('cartkey').'" />';
								echo '<input type="hidden" name="id" value="'.$package->id.'"/>';
								echo '<button type="submit" class="btn btn-gold btn-block">BUY</button>';
						echo '</form>';
						echo '</li>';
					}
				?>
			</ul>

<script type="text/javascript">
csjo.init();
</script>
<?php include('footer.php'); ?>