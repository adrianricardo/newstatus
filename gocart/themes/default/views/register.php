<?php
$additional_header_info = '<style type="text/css">#gc_page_title {text-align:center;}</style>';
include('header.php'); ?>
<?php
$company	= array('id'=>'bill_company', 'placeholder'=>'company', 'class'=>'full', 'name'=>'company', 'value'=> $form_company);
$first		= array('id'=>'bill_firstname', 'placeholder'=>'first', 'name'=>'firstname', 'value'=> $form_firstname);
$last		= array('id'=>'bill_lastname', 'placeholder'=>'last', 'name'=>'lastname', 'value'=> $form_lastname);
$email		= array('id'=>'bill_email', 'placeholder'=>'email', 'name'=>'email', 'value'=> $form_email);
$phone		= array('id'=>'bill_phone', 'placeholder'=>'phone', 'name'=>'phone', 'value'=> $form_phone);
?>
<div class="row login double">
	<div class="form-container">
 		<h1>Register</h1>
 		<?php $attributes = array('class' => 'form-horizontal'); ?>
 		<?php echo form_open('secure/register', $attributes); ?>
			<input type="hidden" name="submitted" value="submitted" />
			<input type="hidden" name="redirect" value="<?php echo $redirect; ?>" />

			<fieldset>
				<div>	
					<div class="span3">
 						<?php echo form_input($first);?>
					</div>
				
					<div class="span3">
 						<?php echo form_input($last);?>
					</div>
				</div>
			
				<div>
					<div class="span3">
 						<?php echo form_input($email);?>
					</div>
				
					<div class="span3">
 						<?php echo form_input($phone);?>
					</div>
				</div>
			
				<div>
					<div class="span7">
						<label class="checkbox">
							<input type="checkbox" name="email_subscribe" value="1" <?php echo set_radio('email_subscribe', '1', TRUE); ?>/> Subscribe to our newsletter for exclusive sales and coupons
						</label>
					</div>
				</div>
			
				<div>	
					<div class="span3">
						<input type="password" name="password" placeholder="password" value="" class="span3"/>
					</div>

					<div class="span3">
						<input type="password" name="confirm" placeholder="confirm" value="" class="span3"/>
					</div>
				</div>
				<div class="span6">
					<input type="submit" value="<?php echo lang('form_register');?>" class="btn btn-gold btn-full" />
					<a class="secondary" href="<?php echo site_url('secure/login'); ?>">Already registered? Login</a>
				</div>
			</fieldset>
		</form>
	
		<div style="text-align:center;">
			
		</div>
	</div>
 </div>
<?php include('footer.php');