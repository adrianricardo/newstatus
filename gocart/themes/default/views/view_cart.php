<?php include('header.php');?>

<?php if ($this->go_cart->total_items()==0):?>
	<div class="alert alert-info">
		<a class="close" data-dismiss="alert">×</a>
		<?php echo lang('empty_view_cart');?>
	</div>
<?php else: ?>
	

	<?php echo form_open('cart/update_cart', array('id'=>'update_cart_form'));?>
	<div id="cart-header">
		<?php if(!$this->Customer_model->is_logged_in(false,false)): ?>
		<div id="login-block">
			<div id="login-image"><i class="icon-user"></i></div>
			<div id="login-text">
				<div>Login for a faster checkout</div>
				<a href="/secure/register">Register.</a> 
				<a class="facebookLogin" href="#">Connect with facebook.</a>
			</div>
		</div>
		<?php endif; ?>
		<div id="coupon-block">
			<?php // <label>Need a code?</label> ?>
			<div class="input-append">
			  <input type="text" name="coupon_code" class="span3" style="margin:0px;">
			  <input class="span2 btn" type="submit" value="<?php echo lang('apply_coupon');?>"/>
			</div>
		</div>
	</div>
	
	<?php include('checkout/summary.php');?>

	<div id="checkout-block">
		<input id="redirect_path" type="hidden" name="redirect" value=""/>
		<?php if ($this->Customer_model->is_logged_in(false,false) || !$this->config->item('require_login')): ?>
			<input id="checkout" type="submit" onclick="$('#redirect_path').val('checkout');" value="<?php echo lang('form_checkout');?>"/>
		<?php endif; ?>
		<table class="table">
			<?php
			/**************************************************************
			Subtotal Calculations
			**************************************************************/
			?>
			<?php if($this->go_cart->group_discount() > 0)  : ?> 
        	<tr>
        		<td colspan="2">
				<td colspan="1"><strong><?php echo lang('group_discount');?></strong></td>
				<td><?php echo format_currency(0-$this->go_cart->group_discount()); ?>                </td>
			</tr>
			<?php endif; ?>
			
			<tr>
				<td colspan="2">
		    	<td colspan="1"><strong><?php echo lang('subtotal');?></strong></td>
				<td id="gc_subtotal_price"><?php echo format_currency($this->go_cart->subtotal()); ?></td>
			</tr>
			
				
			<?php if($this->go_cart->coupon_discount() > 0) {?>
		    <tr>
		    	<td colspan="2">
		    	<td colspan="1"><strong><?php echo lang('coupon_discount');?></strong></td>
				<td id="gc_coupon_discount">-<?php echo format_currency($this->go_cart->coupon_discount());?></td>
			</tr>
				<?php if($this->go_cart->order_tax() != 0) { // Only show a discount subtotal if we still have taxes to add (to show what the tax is calculated from)?> 
				<tr>
					<td colspan="2">
		    		<td colspan="1"><strong><?php echo lang('discounted_subtotal');?></strong></td>
					<td id="gc_coupon_discount"><?php echo format_currency($this->go_cart->discounted_subtotal());?></td>
				</tr>
				<?php
				}
			} 
			/**************************************************************
			 Custom charges
			**************************************************************/
			$charges = $this->go_cart->get_custom_charges();
			if(!empty($charges))
			{
				foreach($charges as $name=>$price) : ?>
					
			<tr>
				<td colspan="2">
				<td colspan="1"><strong><?php echo $name?></strong></td>
				<td><?php echo format_currency($price); ?></td>
			</tr>	
					
			<?php endforeach;
			}	


			/**************************************************************
			 Custom charges
			**************************************************************/

			$insurance = $this->go_cart->get_insurance();
			if($insurance > 0) : ?>
			<tr>
				<td colspan="2">
				<td colspan="1"><strong>USPS Insurance</strong></td>
				<td><?php echo format_currency($insurance); ?></td>
			</tr>	
					
			<?php endif; 	
			
			
			/**************************************************************
			Order Taxes
			**************************************************************/
			 // Show shipping cost if added before taxes
			if($this->config->item('tax_shipping') && $this->go_cart->shipping_cost()>0) : ?>
				<tr>
				<td colspan="2">
				<td colspan="1"><strong><?php echo lang('shipping');?></strong></td>
				<td><?php echo format_currency($this->go_cart->shipping_cost()); ?></td>
			</tr>
			<?php endif;
			if($this->go_cart->order_tax() > 0) :  ?>
		    <tr>
		    	<td colspan="2">
		    	<td colspan="1"><strong><?php echo lang('tax');?></strong></td>
				<td><?php echo format_currency($this->go_cart->order_tax());?></td>
			</tr>
			<?php endif; 
			// Show shipping cost if added after taxes
			if(!$this->config->item('tax_shipping') && $this->go_cart->shipping_cost()>0) : ?>
				<tr>
				<td colspan="2">
				<td colspan="1"><strong><?php echo lang('shipping');?></strong></td>
				<td><?php echo format_currency($this->go_cart->shipping_cost()); ?></td>
			</tr>
			<?php endif ?>
			
			<?php
			/**************************************************************
			Gift Cards
			**************************************************************/
			if($this->go_cart->gift_card_discount() > 0) : ?>
			<tr>
				<td colspan="2">
				<td colspan="1"><strong><?php echo lang('gift_card_discount');?></strong></td>
				<td>-<?php echo format_currency($this->go_cart->gift_card_discount()); ?></td>
			</tr>
			<?php endif; ?>
			
			<?php
			/**************************************************************
			Grand Total
			**************************************************************/
			?>

			
			<tr>
				<td colspan="2">
				<td colspan="1"><strong><?php echo lang('grand_total');?></strong></td>
				<td><?php echo format_currency($this->go_cart->total()); ?></td>
			</tr>
				
		</table>
	</div>
	
	<div class="single-line-input" id="gift-cards">
		<div class="span5 input-append">
			<?php if($gift_cards_enabled):?>
				<label style="margin-top:15px;"><?php echo lang('gift_card_label');?></label>
				<input type="text" name="gc_code" class="span3" style="margin:0px;">
				<input class="span2 btn"  type="submit" value="<?php echo lang('apply_gift_card');?>"/>
			<?php endif;?>
		</div>
	</div>

</form>
<?php endif; ?>

<script type='text/javascript'>
	$(function(){
		csjo.initCart(); 
	})
</script>