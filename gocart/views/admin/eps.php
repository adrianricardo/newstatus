<?php include('header.php'); ?>
<style media="screen" type="text/css">

.small-img{
	max-height: 100px;
	max-width: 100px;
}
.set-category{
	position: fixed;
	right: 170px;
	top: 110px;
}
tbody .checkbox{
	width: 100px;
	height: 100px;
}

</style>
<script type="text/javascript">
$('#quick-cateogry').on('submit',function(e){
	e.preventDefault();
	if ($('#parent-id').val() == "0"){
		alert('choose a category type playa');
		return false;
	}
	if ($('#name').val() == ""){
		alert('add a category name playa');
		return false;
	}
	$(this)[0].submit();
})
</script>
<div class="set-category">
	<ul class="nav nav-tabs">
	  <li><a href="#new-cat" data-toggle="tab">New Brand/Category</a></li>
 	  <li><a href="#set-cat" data-toggle="tab">Set Brand/Category</a></li>
	</ul>
	<div class="tab-content">
  		<div class="tab-pane" id="new-cat">
  			<h6>Add a new brand or category</h6>
  			<form id="quick-cateogry" class="form" action="<?php echo $admin_url;?>categories/quick_category" METHOD="POST">
				<select id="parent-id" name="parent_id"> 
					<option value="0">SELECT TYPE</option>
					<option value="4">Brand</option>
					<option value="1">Category</option>
				</select>
				<input type="text" name="name" />
				<input type="submit" class="btn btn-primary" />
			</form>
  		</div>
  		<div class="tab-pane active" id="set-cat">
  			<h6>Categorize items</h6>
  			<form class="form" action="set_categories" METHOD="POST">
				<select name="category">
					<?php foreach ($categories as $category): ?>
						<option value="<?php echo $category->id; ?>"><?php echo $category->slug; ?></option>
					<?php endforeach; ?>
				</select>
				<input type="submit" class="btn btn-primary" />
  		</div>
  	</div>
	
</div>
<div class="row" style="width:650px;">
	<table class="table table-stripled" style="width:500px">
		<thead>
			<tr>
				<td width="100px">image</td>
				<td>name</td>
				<td>brand</td>
				<td>category</td>
				<td>select</td>
			</tr>
		</thead>
		<?php foreach ($products as $product): ?>
			<tr>
				<td>
					<?php $product->images	= (array)json_decode($product->images);
					foreach ($product->images as $img) :
						if (isset($img->filename)):
					 		$photo	= '<img class="small-img" src="'.base_url('uploads/images/small/'.$img->filename).'"/>';
					 		break;
					 	else:
					 		$photo	= '<img src="'.theme_img('no_picture.png').'" alt="'.$product->seo_title.'" height="100"/>';
					 	endif;
					endforeach; ?>
					<a class="thumbnail" href="<?php echo base_url('/'.$product->slug); ?>">
						<?php echo $photo; ?>
					</a>
				</td>
				<td><?php echo $product->name;?></td>
				<td><?php echo $product->brand;?></td>
				<td><?php echo $product->category;?></td>
				<td><label for="product[<?php echo $product->id;?>]"><input class="checkbox" type="checkbox" name="product[<?php echo $product->id;?>]" id="<?php echo $product->id;?>"></label></td>
			</tr>
		<?php endforeach; ?>
	</table></form>
</div>

		
<?php include('footer.php'); ?>